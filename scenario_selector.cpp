/***************************************************************************
                          Scenario_selector.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "Main.h"
#include "scenario_selector.h"

scenario_selector::scenario_selector(Kursk*_app)
{
	app=_app;
	list_top=-3;
	list_bottom=2;
}

CL_String read_line(CL_InputSource*in)
{
	CL_String ret;
	static char c[2]={ 0,0 };

	do
	{
		c[0]=in->read_char8();
		switch(c[0])
		{
		case 13:
			c[0]=in->read_char8();
			break;
		case 10:
			break;
		default:
			ret+=c;
			break;
		}
	} while(c[0]!=10);

	return ret;
}

void scenario_selector::load_scenario_list(CL_String list_path)
{

	CL_InputSourceProvider*input=CL_InputSourceProvider::create_file_provider("");
	CL_InputSource*file=input->open_source(list_path);

	bool a=true;
	while(a)
	{
		CL_String fname=read_line(file);
		if(fname=="#end")
		{
			a=false;
		}
		else
			scenario.push_back(new scenario_list_entry(fname));
	}
}

void scenario_selector::show_scenario_list()
{
	int j=0;
	int i=0;
	int k;
	int l=0;
	int length;

	for(std::list<scenario_list_entry*>::iterator t_list=scenario.begin();
	t_list!=scenario.end();t_list++)
	{
		if(i>=list_top&&i<=list_bottom)
		{
			if((*t_list)->selected())
			{
				CL_Display::fill_rect(66,64+(j*12),268,(64+(j*12))+12,0.0f,0.3f,0.0f,0.6f);
				switch (app->scenario_number)
				{
				case 0:
					app->scenario_description = CL_String(CL_Res_String::load("ScenarioDescription/scenario1",app->resources));
					scen_map=CL_Surface::load("Graphics/Interface/scen_map",app->resources);
					length=app->scenario_description.get_length();
					scen_map->put_screen(282,61);
					for (k=0;k<=length;k+=30)
					{
						app->simple_font_yellow->print_left(66,(320+12*l),app->scenario_description.mid(k,30));
						l++;
					}
					break;
				case 1:
					app->scenario_description = CL_String(CL_Res_String::load("ScenarioDescription/scenario2",app->resources));
					length=app->scenario_description.get_length();
					for (k=0;k<=length;k+=30)
					{
						app->simple_font_yellow->print_left(66,(320+12*l),app->scenario_description.mid(k,30));
						l++;
					}
					break;
				case 2:
					app->scenario_description = CL_String(CL_Res_String::load("ScenarioDescription/scenario3",app->resources));
					length=app->scenario_description.get_length();
					for (k=0;k<=length;k+=30)
					{
						app->simple_font_yellow->print_left(66,(320+12*l),app->scenario_description.mid(k,30));
						l++;
					}
					break;
				}
			}

			CL_String name=(*t_list)->get_name();
			if (j==0)
				app->simple_font->print_left(66,66+(j*12),name.get_string());
			else
				app->simple_font_grey->print_left(66,66+(j*12),name.get_string());
			j++;
		}
		i++;
	}
}

void scenario_selector::check_input()
{
	// left_click on scenario name
	if(CL_Mouse::left_pressed())
	{
		for(std::list<scenario_list_entry*>::iterator t_list=scenario.begin();
			t_list!=scenario.end();
			t_list++)
		{
			if((*t_list)->selected()==true)
				(*t_list)->set_selected(false);
		}

		if(CL_Mouse::get_x()>66&&CL_Mouse::get_x()<268
			&&CL_Mouse::get_y()>66&&CL_Mouse::get_y()<300)
		{
			int selection=(((int)CL_Mouse::get_y()-12)/12);
			selection+=list_top-1;
			
			// store the selected scenario number
			app->scenario_number=selection;
			// assign the req.points for the selected scenario
			switch (selection)
			{
			case 0:
				app->requisition_point=120;
				app->scenario_turn=12;
				app->r_is_artillery=false;
				app->g_is_artillery=false;
				break;
			case 1:
				app->requisition_point=105;
				break;
			case 2:
				app->requisition_point=95;
				break;			
			}

			selection=0;		//forcing first scenario

			std::list<scenario_list_entry*>::iterator it=scenario.begin();
			for(int i=0; i<selection; i++)
				it++;

			if(selection<scenario.size()&&selection>-1)
			{
				if((*it)->selected()==false)
				{
					app->selected_scenario.push_back((*it));
					(*it)->set_selected(true);
				}
			}
		}
	}
}

void scenario_selector::draw()
{
	app->scenario_background->put_screen(0,0);
	app->simple_font->print_center(320,465,"After selecting your scenario, press the Select Scenario button.");
	show_scenario_list();
}

scenario_list_entry::scenario_list_entry(CL_String _name)
{
	scenario_name=_name;
	t_selected=false;
}



