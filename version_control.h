/***************************************************************************
                          version_control.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _VERSION_CONTROL_H_
#define _VERSION_CONTROL_H_
#define FC __fastcall

#define APP_NAME		"Operation Citadel"
#define APP_VER_MAJOR	0
#define APP_VER_MINOR	1
#define APP_BUILD		1

#undef FC
#endif