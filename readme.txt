Operation Citadel is a WWII, turn based platoon, company level war game on the Eastern Front. The game is somewhere in between CLose Combat� and Steel Panthers�. 

The game takes you to the biggest tank battle of the Second World War, to the vicinity of Prohorovka in the Kursk salient as a platoon leader of the 1st Kompanie, 1st Abteilung, Panzer Regiment 1 of the 1st LSAH Panzergrenadier Division. The game is in an early beta stage and it's playable already. 

The game is written in C++, using the Clanlib Game SDK. The kursk_source.zip file consist of the MSVC 6.0 project file with all the necessary source code.

In order to successfully compile the MSVC 6.0 Project, first you will have to install the ClanLib Game SDK v0.4.4 or higher and of course, you will need the resource file which stores the game graphics and sound files. You will need WINRAR in order to unpack the resource file. You can get it here: http://www.rarsoft.com.

(C) 2000 ByteWare Studio
