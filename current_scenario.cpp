// current_scenario.cpp: implementation of the current_scenario class.
//
//////////////////////////////////////////////////////////////////////
#include "Main.h"
#include "current_scenario.h"
#include "map.h"

class scenario_list_entry;

current_scenario::current_scenario(Kursk*_app)
{
	app=_app;
}


void current_scenario::init_scenario()
{
	std::cout<<"init objects"<<std::endl;
	app->scenario_map=new tileMap();
	std::cout<<"created tileMap()"<<std::endl;

	int num_selected_scenario=scenario.size();
   
    if( scenario_number >  num_selected_scenario ) scenario_number = 0;
    if( scenario_number >= 0 && scenario_number <= num_selected_scenario )
	{
	    CL_String path;
	    path+="scenarios/";

	    std::list<scenario_list_entry*>::iterator t_list=scenario.begin();
	    for(int i=0; i<track_nr; i++)
			t_list++;

	    path+=(*t_list)->get_name();

	    scenario_map->load_map(path);

	    std::cout << "map loaded succesfully" << std::endl;
	}
}

void current_scenario::deployment()
{
	//setup the scenario interface and draw the map
	init_scenario();

}
