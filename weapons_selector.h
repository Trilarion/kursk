/***************************************************************************
                          Weapons_selector.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _WEAPONS_SELECTOR_H_
#define _WEAPONS_SELECTOR_H_
#define FC __fastcall

class weapons_list_entry;
 
class weapons_selector
{
public:
	weapons_selector(Kursk *_app);

	void load_weapons_list(CL_String list_path);
	void check_input();
	void draw();

protected:
	void show_weapons_list();
	Kursk *app;
	std::list<weapons_list_entry*> weapons;
	int list_top;
	int list_bottom;
	char unit_description[8][45];
};

class weapons_list_entry
{
public:
	weapons_list_entry(CL_String);

	CL_String get_name() {return weapons_name;}
   
	bool selected() {return t_selected;}
	void set_selected(bool sel) { t_selected=sel;}
   
protected:
	CL_String weapons_name;
	bool t_selected;
};

#undef FC
#endif
