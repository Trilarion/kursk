/***************************************************************************
                          Math.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _MATH_H_
#define _MATH_H_

typedef unsigned char  Uint8,  U8,  * U8ptr;
typedef unsigned short Uint16, U16, * U16ptr;
typedef unsigned int   Uint32, U32, * U32ptr;
typedef unsigned long  Ulong,  UL,  * ULptr;

//typedef          char  Sint8,  S8,  * S8ptr;
typedef          short Sint16, S16, * S16ptr;
typedef          int   Sint32, S32, * S32ptr;
typedef          long  Slong,  SL,  * SLptr;

typedef			 float FL, F32, *Fptr;
typedef			 double DB, D80, *Dptr;

typedef			 void* VoidPtr;

//*** particle ***//
typedef struct PARTICLE
{
	FL		x, y;			// screen position
	FL		Velocity;		// speed
	U32		Direction, Time;// integer based direction, time started in millisecs
	U8		Counter, Brightness;

	PARTICLE () {Clear ();}
	void  Clear () { x = 0.0f, y = 0.0f, Velocity = 0.0f, Direction = 0, Time = 0; Counter = 4; Brightness = 255;}
}PARTICLE, *PARTICLEptr, **PARTICLElist;

template <typename type>					// RADtoDEG
type  RADtoDEG (type val) {return val * 57.2957795132;}
template <typename type>					// MIN
type  MIN (type a, type b) {return ((a<b) ? a:b);}   
template <typename type>					// MAX
type  MAX (type a, type b) {return ((a>b) ? a:b);}    

class Math
{
public:
	Math(Kursk* app);

	static int distance(int source_x,int source_y,int target_x,int target_y);
	static int obstacle_height(Kursk* app,int source_x,int source_y,int target_x,int target_y);
	static void load_data(GameObject* source);
	static void scenario_result(Kursk* app);
};

#endif