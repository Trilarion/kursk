/***************************************************************************
                          Enemy.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <time.h>
#include "Main.h"
#include "Objects.h"
#include "Map.h"
#include "Projectile.h"
#include "Interface.h"
#include "Math.h"
#include "Enemy.h"
#include "Miscellaneous.h"
#include "Path.h" 
#include "Random.h"

void Enemy::enemy_ai(Kursk* app)
{
	int i,fuzzy_total,die_roll;
	
	fuzzy_total=0;

	for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
	{
		if (!(*it)->is_german && (*it)->object_status==0 && !(*it)->is_dead)
		{
			(*it)->enemy_number=(*it)->enemy_seen(app);

			//the unit saw some enemy
			if ((*it)->enemy_number>0 && (app->game_state!=r_fire || 
				app->game_state!=r_do_fire))
			{
				//which unit saw enemies
				switch ((*it)->object_index)
				{
				case 0:				//T-34 tank three of them

				case 1:

				case 2:					
					for (i=0;i<(*it)->enemy_number;i++)
					{
						{for (std::list<GameObject*>::iterator item=app->objects.begin();item!=app->objects.end();item++)
							if ((*item)->is_german && (*item)->object_index==(*it)->enemy_index[i])	
							{
								switch ((*item)->object_id)
								{
								case 0:			//Pz.IVJ tank
									if ((*it)->shot_maingun+(*it)->shot_coax>0 && (app->game_state!=r_fire || 
										app->game_state!=r_do_fire))
									{
										(*it)->is_fired=false;
										app->random_number.RandomInit(time(0));
										app->random_number.SetInterval(1,100);
										die_roll=app->random_number.iRandom();

										(*it)->state.move=5;
										(*it)->state.attack=75;
										(*it)->state.guard=15;
										(*it)->state.duck=5;
										app->random_number.SetInterval(1,8);
										if (Math::distance((*it)->x,(*it)->y,(*it)->enemy_x[i],(*it)->enemy_y[i]) <= app->random_number.iRandom() &&
											die_roll>0 && die_roll<(*it)->state.attack)
										{
											(*it)->is_firing=true;
											(*it)->enemy_fired_at=i;
											(*it)->is_fired=true;
											app->game_state=r_fire;
											if ((*it)->shot_maingun+(*it)->shot_coax>0)
												Analysis::r_target_eliminated((*it),(*item),app);
											else
												(*it)->is_finished_firing=true;
										}
									}
									
									break;

								case 1:			//german infantry
									if ((*it)->shot_maingun+(*it)->shot_coax>0 && (app->game_state!=r_fire || 
										app->game_state!=r_do_fire))
									{
										(*it)->is_fired=false;
										app->random_number.RandomInit(time(0));
										app->random_number.SetInterval(1,100);
										die_roll=app->random_number.iRandom();

										(*it)->state.move=0;
										(*it)->state.attack=90;
										(*it)->state.guard=5;
										(*it)->state.duck=5;
										app->random_number.SetInterval(1,8);
										if (Math::distance((*it)->x,(*it)->y,(*it)->enemy_x[i],(*it)->enemy_y[i]) <= app->random_number.iRandom() &&
											die_roll>0 && die_roll<(*it)->state.attack)
										{
											(*it)->is_firing=true;
											(*it)->enemy_fired_at=i;
											(*it)->is_fired=true;
											app->game_state=r_fire;
											if ((*it)->shot_maingun+(*it)->shot_coax>0)
												Analysis::r_target_eliminated((*it),(*item),app);
											else
												(*it)->is_finished_firing=true;
										}
									}

									break;	

								case 2:			//Marder IIIG
									if ((*it)->shot_maingun+(*it)->shot_coax>0 && (app->game_state!=r_fire || 
										app->game_state!=r_do_fire))
									{
										(*it)->is_fired=false;
										app->random_number.RandomInit(time(0));
										app->random_number.SetInterval(1,100);
										die_roll=app->random_number.iRandom();

										(*it)->state.move=20;
										(*it)->state.attack=60;
										(*it)->state.guard=10;
										(*it)->state.duck=10;
										app->random_number.SetInterval(1,10);
										if (Math::distance((*it)->x,(*it)->y,(*it)->enemy_x[i],(*it)->enemy_y[i]) <= app->random_number.iRandom() &&
											die_roll>0 && die_roll<(*it)->state.attack)
										{
											(*it)->is_firing=true;
											(*it)->enemy_fired_at=i;
											(*it)->is_fired=true;
											app->game_state=r_fire;
											if ((*it)->shot_maingun+(*it)->shot_coax>0)
												Analysis::r_target_eliminated((*it),(*item),app);
											else
												(*it)->is_finished_firing=true;
										}
									}
									
									break;
								}
							}
						}
					}

					break;

				case 3:					//T-70 tank (two of them)

				case 4:			
					for (i=0;i<(*it)->enemy_number;i++)
					{
						for (std::list<GameObject*>::iterator item=app->objects.begin();item!=app->objects.end();item++)
						{
							if ((*item)->is_german && (*item)->object_index==(*it)->enemy_index[i])	
							{
								switch ((*item)->object_id)
								{
								case 0:			//Pz.IVJ tank
									if ((*it)->shot_maingun+(*it)->shot_coax>0 && (app->game_state!=r_fire || 
										app->game_state!=r_do_fire))
									{
										(*it)->is_fired=false;
										app->random_number.RandomInit(time(0));
										app->random_number.SetInterval(1,100);
										die_roll=app->random_number.iRandom();

										(*it)->state.move=30;
										(*it)->state.attack=45;
										(*it)->state.guard=5;
										(*it)->state.duck=20;
										app->random_number.SetInterval(1,5);
										if (Math::distance((*it)->x,(*it)->y,(*it)->enemy_x[i],(*it)->enemy_y[i]) <= app->random_number.iRandom() &&
											die_roll>0 && die_roll<(*it)->state.attack)
										{
											(*it)->is_firing=true;
											(*it)->enemy_fired_at=i;
											(*it)->is_fired=true;
											app->game_state=r_fire;
											if ((*it)->shot_maingun+(*it)->shot_coax>0)
												Analysis::r_target_eliminated((*it),(*item),app);
											else
												(*it)->is_finished_firing=true;
										}
									}

									break;

								case 1:			//german infantry
									if ((*it)->shot_maingun+(*it)->shot_coax>0 && (app->game_state!=r_fire || 
										app->game_state!=r_do_fire))
									{
										(*it)->is_fired=false;
										app->random_number.RandomInit(time(0));
										app->random_number.SetInterval(1,100);
										die_roll=app->random_number.iRandom();

										(*it)->state.move=5;
										(*it)->state.attack=60;
										(*it)->state.guard=20;
										(*it)->state.duck=15;
										app->random_number.SetInterval(1,6);
										if (Math::distance((*it)->x,(*it)->y,(*it)->enemy_x[i],(*it)->enemy_y[i]) <= app->random_number.iRandom() &&
											die_roll>0 && die_roll<(*it)->state.attack)
										{
											(*it)->is_firing=true;
											(*it)->enemy_fired_at=i;
											(*it)->is_fired=true;
											app->game_state=r_fire;
											if ((*it)->shot_maingun+(*it)->shot_coax>0)
												Analysis::r_target_eliminated((*it),(*item),app);
											else
												(*it)->is_finished_firing=true;
										}
									}

									break;	

								case 2:			//Marder IIIG
									if ((*it)->shot_maingun+(*it)->shot_coax>0 && (app->game_state!=r_fire || 
										app->game_state!=r_do_fire))
									{
										(*it)->is_fired=false;
										app->random_number.RandomInit(time(0));
										app->random_number.SetInterval(1,100);
										die_roll=app->random_number.iRandom();

										(*it)->state.move=45;
										(*it)->state.attack=30;
										(*it)->state.guard=15;
										(*it)->state.duck=10;
										app->random_number.SetInterval(1,8);
										if (Math::distance((*it)->x,(*it)->y,(*it)->enemy_x[i],(*it)->enemy_y[i]) <= app->random_number.iRandom() &&
											die_roll>0 && die_roll<(*it)->state.attack)
										{
											(*it)->is_firing=true;
											(*it)->enemy_fired_at=i;
											(*it)->is_fired=true;
											app->game_state=r_fire;
											if ((*it)->shot_maingun+(*it)->shot_coax>0)
												Analysis::r_target_eliminated((*it),(*item),app);
											else
												(*it)->is_finished_firing=true;
										}
									}
									
									break;
								}
							}
						}
					}

					break;

				case 5:					//120mm Mortar
					//later...
					break;

				case 6:					//Infantry (all three of them)
					
				case 7:
					
				case 8:
					for (i=0;i<(*it)->enemy_number;i++)
					{
						for (std::list<GameObject*>::iterator item=app->objects.begin();item!=app->objects.end();item++)
						{
							if ((*item)->is_german && (*item)->object_index==(*it)->enemy_index[i])	
							{
								switch ((*item)->object_id)
								{
								case 0:			//Pz.IVJ tank
									if ((*it)->shot_maingun+(*it)->shot_coax>0 && (app->game_state!=r_fire || 
										app->game_state!=r_do_fire))
									{
										(*it)->is_fired=false;
										app->random_number.RandomInit(time(0));
										app->random_number.SetInterval(1,100);
										die_roll=app->random_number.iRandom();

										(*it)->state.move=35;
										(*it)->state.attack=35;
										(*it)->state.guard=15;
										(*it)->state.duck=15;
										app->random_number.SetInterval(1,3);
										if (Math::distance((*it)->x,(*it)->y,(*it)->enemy_x[i],(*it)->enemy_y[i]) <= app->random_number.iRandom() &&
											die_roll>0 && die_roll<(*it)->state.attack)
										{
											(*it)->is_firing=true;
											(*it)->enemy_fired_at=i;
											(*it)->is_fired=true;
											app->game_state=r_fire;
											if ((*it)->shot_maingun+(*it)->shot_coax>0)
												Analysis::r_target_eliminated((*it),(*item),app);
											else
												(*it)->is_finished_firing=true;
										}
									}
									
									break;

								case 1:			//german infantry
									if ((*it)->shot_maingun+(*it)->shot_coax>0 && (app->game_state!=r_fire || 
										app->game_state!=r_do_fire))
									{
										(*it)->is_fired=false;
										app->random_number.RandomInit(time(0));
										app->random_number.SetInterval(1,100);
										die_roll=app->random_number.iRandom();

										(*it)->state.move=10;
										(*it)->state.attack=65;
										(*it)->state.guard=15;
										(*it)->state.duck=10;
										app->random_number.SetInterval(1,8);
										if (Math::distance((*it)->x,(*it)->y,(*it)->enemy_x[i],(*it)->enemy_y[i]) <= app->random_number.iRandom() &&
											die_roll>0 && die_roll<(*it)->state.attack)
										{
											(*it)->is_firing=true;
											(*it)->enemy_fired_at=i;
											(*it)->is_fired=true;
											app->game_state=r_fire;
											if ((*it)->shot_maingun+(*it)->shot_coax>0)
												Analysis::r_target_eliminated((*it),(*item),app);
											else
												(*it)->is_finished_firing=true;
										}
									}
									
									break;	

								case 2:			//Marder IIIG
									if ((*it)->shot_maingun+(*it)->shot_coax>0&& (app->game_state!=r_fire || 
										app->game_state!=r_do_fire))
									{
										(*it)->is_fired=false;
										app->random_number.RandomInit(time(0));
										app->random_number.SetInterval(1,100);
										die_roll=app->random_number.iRandom();

										(*it)->state.move=10;
										(*it)->state.attack=80;
										(*it)->state.guard=5;
										(*it)->state.duck=5;
										app->random_number.SetInterval(1,3);
										if (Math::distance((*it)->x,(*it)->y,(*it)->enemy_x[i],(*it)->enemy_y[i]) <= app->random_number.iRandom() &&
											die_roll>0 && die_roll<(*it)->state.attack)
										{
											(*it)->is_firing=true;
											(*it)->enemy_fired_at=i;
											(*it)->is_fired=true;
											app->game_state=r_fire;
											if ((*it)->shot_maingun+(*it)->shot_coax>0)
												Analysis::r_target_eliminated((*it),(*item),app);
											else
												(*it)->is_finished_firing=true;
										}
									}
									
									break;
								}
							}
						}
					}
					break;
				}
				break;		//get out from the "for" loop
			}
		}
	}
}

int Enemy::enemy_movement(Kursk* app)
{
	Enemy::initialize_influence_map(app);
	Enemy::generate_influence_map(app);
	
	srand((unsigned)time(NULL));

	{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
		//bailed-out crews are moving off map
		//routed units and tanks with damaged main gun as well 
		if (!(*it)->is_german && !(*it)->is_dead && !(*it)->is_moved &&
			((*it)->object_id==4 || (*it)->object_status==1 ||
			(*it)->object_status==3))
		{
			(*it)->is_visible=false;
			if ((*it)->object_id==3)
			{
				switch ((*it)->number_of_soldiers)
					{
					case 1:
						(*it)->shadow=(*it)->shadow_array1[0];
						(*it)->hull=(*it)->hull_array1[0];
						break;
					case 2:
						(*it)->shadow=(*it)->shadow_array2[0];
						(*it)->hull=(*it)->hull_array2[0];
						break;
					case 3:
						(*it)->shadow=(*it)->shadow_array3[0];
						(*it)->hull=(*it)->hull_array3[0];
						break;
					case 4:
						(*it)->shadow=(*it)->shadow_array4[0];
						(*it)->hull=(*it)->hull_array4[0];
						break;
					case 5:
						(*it)->shadow=(*it)->shadow_array5[0];
						(*it)->hull=(*it)->hull_array5[0];
						break;
					case 6:
						(*it)->shadow=(*it)->shadow_array6[0];
						(*it)->hull=(*it)->hull_array6[0];
						break;
					case 7:
						(*it)->shadow=(*it)->shadow_array7[0];
						(*it)->hull=(*it)->hull_array7[0];
						break;
					case 8:
						(*it)->shadow=(*it)->shadow_array8[0];
						(*it)->hull=(*it)->hull_array8[0];
						break;
					case 9:
						(*it)->shadow=(*it)->shadow_array9[0];
						(*it)->hull=(*it)->hull_array9[0];
						break;
					case 10:
						(*it)->shadow=(*it)->shadow_array[0];
						(*it)->hull=(*it)->hull_array[0];
						break;
					}
			}
			else
			{
				(*it)->hull=(*it)->hull_array[0];
				(*it)->shadow=(*it)->shadow_array[0];
			}
			if ((*it)->is_turret)
				(*it)->turret=(*it)->turret_array[0];
			app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';
			(*it)->x=(*it)->x+96;
			//check if the tile is free
			if (app->scenario_map->get_drive_dir((*it)->x/TILE_WIDTH,(*it)->y/TILE_HEIGHT)=='#')
			{
				(*it)->x=(*it)->x+48;
			}
			app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
			//crew reached the edge of the map
			if ((*it)->x>=MAP_WIDTH*TILE_WIDTH)
			{
				(*it)->is_visible=false;
				//(*it)->is_dead=true;
				(*it)->object_status=9;		//out of this scenario
			}

			(*it)->is_moved=true;
		}
	}

	{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
		//all other units
		if (!(*it)->is_german && !(*it)->is_dead && !(*it)->is_moved && 
			(*it)->object_id!=4 && (*it)->object_status!=1 && (*it)->object_status!=3)		
		{
			//AI decision making process
			switch ((*it)->object_index)
			{
			case 0:		//T-34	

				(*it)->is_visible=false;
				if (app->scenario_map->get_influence_map_data(1824/TILE_WIDTH,1104/TILE_HEIGHT)>0.0 &&
					app->scenario_map->get_influence_map_data(1824/TILE_WIDTH,1104/TILE_HEIGHT)<=5.0 &&
					!(*it)->do_astar)	//enemy is close, but not dangerously
				{
					(*it)->target_x=((*it)->x/TILE_WIDTH-2+rand()%5+1)*TILE_WIDTH;
					(*it)->target_y=((*it)->y/TILE_WIDTH-2+rand()%5+1)*TILE_HEIGHT;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->get_influence_map_data(1824/TILE_WIDTH,1104/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)
				{
					(*it)->target_x=1824;
					(*it)->target_y=1008;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->victory_flag3 != 0 &&
					app->scenario_map->get_drive_dir(1824/TILE_WIDTH,1104/TILE_HEIGHT)=='|')
				{
					//std::cout << app->scenario_map->get_drive_dir(1824/TILE_WIDTH,1104/TILE_HEIGHT) << std::endl;
					(*it)->target_x=1824;
					(*it)->target_y=1104;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				
				break;

			case 1:		//T-34
				(*it)->is_visible=false;
				if (app->scenario_map->get_influence_map_data(1728/TILE_WIDTH,192/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)	
				{
					(*it)->target_x=1728;
					(*it)->target_y=336;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)	
				{
					(*it)->target_x=1536;
					(*it)->target_y=576;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->get_influence_map_data(1824/TILE_WIDTH,1104/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)	
				{
					(*it)->target_x=1824;
					(*it)->target_y=1008;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->victory_flag2!=0 &&
					app->scenario_map->get_drive_dir(1488/TILE_WIDTH,624/TILE_HEIGHT)=='|')
				{
					(*it)->target_x=1488;
					(*it)->target_y=624;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}

				break;

			case 2:		//T-34
				(*it)->is_visible=false;
				if (app->scenario_map->get_influence_map_data(1728/TILE_WIDTH,192/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)	
				{
					//std::cout << app->scenario_map->get_influence_map_data(1728/TILE_WIDTH,192/TILE_HEIGHT) << std::endl;
					(*it)->target_x=1632;
					(*it)->target_y=384;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)	
				{
					(*it)->target_x=1536;
					(*it)->target_y=720;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->get_influence_map_data(1824/TILE_WIDTH,1104/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)	
				{
					(*it)->target_x=1632;
					(*it)->target_y=912;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->victory_flag3!=0 &&
					app->scenario_map->get_drive_dir(1824/TILE_WIDTH,1104/TILE_HEIGHT)=='|')
				{
					(*it)->target_x=1824;
					(*it)->target_y=1104;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}

				break;

			case 3:		//T-70
				(*it)->is_visible=false;
				if (app->scenario_map->get_influence_map_data(1728/TILE_WIDTH,192/TILE_HEIGHT)>0.0 &&
					app->scenario_map->get_influence_map_data(1728/TILE_WIDTH,192/TILE_HEIGHT)<=5.0 &&
					!(*it)->do_astar)	//enemy is close, but not dangerously
				{
					(*it)->target_x=((*it)->x/TILE_WIDTH-2+rand()%5+1)*TILE_WIDTH;
					(*it)->target_y=((*it)->y/TILE_WIDTH+rand()%5+1)*TILE_HEIGHT;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				else if (app->scenario_map->get_influence_map_data(1728/TILE_WIDTH,192/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)
				{
					(*it)->target_x=1728;
					(*it)->target_y=144;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				else if (app->scenario_map->victory_flag1!=0 &&
					app->scenario_map->get_drive_dir(1728/TILE_WIDTH,192/TILE_HEIGHT)=='|')
				{
					//std::cout << "Here" << std::endl;
					(*it)->target_x=1728;
					(*it)->target_y=192;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}

				break;

			case 4:		//T-70
				(*it)->is_visible=false;
				if (app->scenario_map->victory_flag1!=0 &&
					app->scenario_map->get_drive_dir(1728/TILE_WIDTH,192/TILE_HEIGHT)=='|')
				{
					//std::cout << "Here1" << std::endl;
					(*it)->target_x=1728;
					(*it)->target_y=192;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}

				break;

			case 6:
				(*it)->is_visible=false;
				if (app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)>0.0 &&
					app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)<=5.0 && 
					!(*it)->do_astar)	//enemy is close, but not dangerously
				{
					(*it)->target_x=1584;
					(*it)->target_y=576;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)
				{
					(*it)->target_x=1584;
					(*it)->target_y=624;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);	
				}
				if (app->scenario_map->victory_flag2!=0 &&
					app->scenario_map->get_drive_dir(1488/TILE_WIDTH,624/TILE_HEIGHT)=='|')
				{
					(*it)->target_x=1488;
					(*it)->target_y=624;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}

				break;

			case 7:
				(*it)->is_visible=false;
				if (app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)>0.0 &&
					app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)<=5.0 &&
					!(*it)->do_astar)	//enemy is close, but not dangerously
				{
					(*it)->target_x=1392;
					(*it)->target_y=624;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)
				{
					(*it)->target_x=1536;
					(*it)->target_y=672;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);	
				}

				break;

			case 8:
				(*it)->is_visible=false;
				if (app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)>0.0 &&
					app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)<=5.0 &&
					!(*it)->do_astar)	//enemy is close, but not dangerously
				{
					(*it)->target_x=1536;
					(*it)->target_y=768;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}
				if (app->scenario_map->get_influence_map_data(1488/TILE_WIDTH,624/TILE_HEIGHT)>5.0 &&
					!(*it)->do_astar)
				{
					(*it)->target_x=1488;
					(*it)->target_y=720;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);	
				}
				if (app->scenario_map->victory_flag2!=0 &&
					app->scenario_map->get_drive_dir(1488/TILE_WIDTH,624/TILE_HEIGHT)=='|')
				{
					(*it)->target_x=1488;
					(*it)->target_y=624;

					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';

					(*it)->unit_path=new AStar(app->scenario_map);
					(*it)->do_astar=true;

					//sound effects
					if (!(*it)->move0->is_playing())
						(*it)->move0->play(true);
				}

				break;
			}
		}
	}
	return 0;
}

void Enemy::initialize_influence_map(Kursk* app)
{
	for (int column=0;column<MAP_HEIGHT;column++)
	{
		for (int row=0;row<MAP_WIDTH;row++)
		{
			app->scenario_map->influence_map[column][row]=0.0;
		}
	}
	//std::cout << "Influence map initialized" << std::endl;
}

int Enemy::generate_influence_map(Kursk* app)
{
	int min_x,max_x,min_y,max_y;
	int i,j,x,y;
	float dist;
	double delta;

	for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
	{
		if ((*it)->is_dead)
			(*it)->combat_strength=0;

		switch ((*it)->object_status)
		{
		case 1:		//gun malfunction
			(*it)->combat_strength=0;
			break;
		case 2:		//pinned
			(*it)->combat_strength=1;
			break;
		case 3:		//withdrawing
			(*it)->combat_strength=2;
			break;
		case 4:		//abandoned
			(*it)->combat_strength=0;
			break;
		case 5:		//wreck
			(*it)->combat_strength=0;
			break;
		case 6:		//burning
			(*it)->combat_strength=0;
			break;
		case 7:		//immobilized
			(*it)->combat_strength=4;
			break;
		case 8:		//routed
			(*it)->combat_strength=1;
			break;
		}

		x=(*it)->x/TILE_WIDTH;
		y=(*it)->y/TILE_HEIGHT;
		//(*it)->range=abs((*it)->range);

		min_x=x-3*(*it)->range;
		max_x=x+3*(*it)->range;
		min_y=y-3*(*it)->range;
		max_y=y+3*(*it)->range;

		min_x=min_x > 0 ? min_x : 0;
		max_x=max_x < MAP_WIDTH ? max_x : MAP_WIDTH-1;
		min_y=min_y > 0 ? min_y : 0;
		max_y=max_y < MAP_HEIGHT ? max_y : MAP_HEIGHT-1;

		for (i=min_y;i<=max_y;i++)
		{
			for (j=min_x;j<=max_x;j++)
			{
				//dist=hypot(i-x,j-y);
				dist=sqrt((pow(y-i,2)+pow(x-j,2)));

				if (dist<(*it)->range)
				{
					delta=1.0*(*it)->combat_strength-((0.1*(*it)->combat_strength*dist)/(*it)->range);
				}
				else if (dist<=2*(*it)->range)
				{
					delta=0.9*(*it)->combat_strength-((0.8*(*it)->combat_strength*(dist-(*it)->range))/(*it)->range);
				}
				else if (dist<=3*(*it)->range)
				{
					delta=0.1*(*it)->combat_strength-((0.1*(*it)->combat_strength*(dist-2*(*it)->range))/(*it)->range);
				}
				else
					continue;
				
				if ((*it)->is_german)
					delta=MAX(delta,0.0);
				else
					delta=MIN(delta,0.0);

				app->scenario_map->influence_map[i][j]+=(int)delta;
			}
		}
	}
	//std::cout << "Influence map generated" << std::endl;
	return 0;
}
