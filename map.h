/***************************************************************************
                          Map.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi 
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _MAP_H_
#define _MAP_H_
#define FC __fastcall

#define MAP_WIDTH		40
#define MAP_HEIGHT		30
#define VIEWPORT_WIDTH	11
#define VIEWPORT_HEIGHT	10

class tileMap
{
public:
	tileMap();
	void load_map(CL_String);

	int  get_tmp_map(int x,int y) { return tmp_map[y][x]; }
	int  get_tile_id(int x,int y) { return map[y][x]; }
	int  get_map_width() { return MAP_WIDTH; }
	int  get_map_height() { return MAP_HEIGHT; }

	int  get_w_map(int x,int y) { return water_map[y][x]; }
	void set_w_map(int x,int y) { water_map[y][x]=0; }
	void set_w_map_free(int x,int y) { water_map[y][x]=1; }

	int  get_drive_dir(int x,int y) { return drive_map[y][x]; }
	double get_influence_map_data(int x,int y) {return influence_map[y][x];}

	int  get_data(int x,int y) { return data_map[y][x]; }
	int  get_tmp_data(int x,int y) { return tmp_data_map[y][x]; }

	int  get_s_pos_x(int id) { return s_pos_x[id]; }
	int  get_s_pos_y(int id) { return s_pos_y[id]; }

	int drive_map[MAP_HEIGHT+1][MAP_WIDTH+1];	// obstacle map for pathfinding
	int data_map[MAP_HEIGHT+1][MAP_WIDTH+1];	// obstacle map for LOS
	double influence_map[MAP_HEIGHT][MAP_WIDTH];// influence map for the AI

	int victory_flag1,victory_flag2,victory_flag3;

private:
	int tmp_map[MAP_HEIGHT+1][MAP_WIDTH+1];		// map loaded from file
	int map[MAP_HEIGHT+1][MAP_WIDTH+1];			

	int water_map[MAP_HEIGHT+1][MAP_WIDTH+1];	

	int tmp_data_map[MAP_HEIGHT+1][MAP_WIDTH+1];

	// starting positions
	int s_pos_x[4];
	int s_pos_y[4];
};

#undef FC
#endif
