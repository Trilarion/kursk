/***************************************************************************
                          Miscellaneous.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <time.h>
#include "Main.h"
#include "Objects.h"
#include "Interface.h"
#include "Map.h"
#include "Math.h"
#include "Miscellaneous.h"
#include "Menu.h"
#include "Random.h"

void Analysis::target_eliminated(GameObject* source,GameObject* target,Kursk* app)
{
	int dist,_penetration_value,armor_value,fire_power,location_variable;
	float hit_probability,_to_hit,die_roll, die_roll_six;
	double X1,X2,X3,X4,X5;
	double RJ;
	char* hit_location;

	hit_location="";

	//the distance between two units in cells.
	dist = Math::distance(source->x,source->y,target->x,target->y);

	//1D20 die_roll
	app->random_number.RandomInit(time(0));
	app->random_number.SetInterval(1,20);
	die_roll=app->random_number.iRandom();

	//gun malfunction
	//if (die_roll==20)
	//{
	//	source->object_status=1;

	//	if (source->shot_maingun > 0)
	//		source->shot_maingun=0;
	//	else if (source->shot_coax > 0)
	//		source->shot_coax=0;
	//}

	//to hit variable, depending on the target distance and the firing unit
	switch (source->object_id)
	{
	case 0:						//Pz IVJ

		X3=-0.2;						//firer moving slowly
		X4=0;							//AP round
		if (dist>0 && dist<=2)
		{
			_to_hit=source->to_hit[0];
			_penetration_value=source->penetration_value[0];
			RJ=-0.05;
		}
		else if (dist>2 && dist<=6)
		{
			_to_hit=source->to_hit[1];
			_penetration_value=source->penetration_value[1];
			RJ=-0.1;
		}
		else if (dist>6 && dist<=12)
		{
			_to_hit=source->to_hit[2];
			_penetration_value=source->penetration_value[2];
			RJ=-0.2;
		}
		else if (dist>12 && dist<=18)
		{
			_to_hit=source->to_hit[3];
			_penetration_value=source->penetration_value[3];
			RJ=-0.4;
		}
		else if (dist>18 && dist<=24)
		{
			_to_hit=source->to_hit[4];
			_penetration_value=source->penetration_value[4];
			RJ=-0.5;
		}
		else if (dist>24 && dist<=30)
		{
			_to_hit=source->to_hit[5];
			_penetration_value=source->penetration_value[5];
			RJ=-0.55;
		}
		else if (dist>30 && dist<=36)
		{
			_to_hit=source->to_hit[6];
			_penetration_value=source->penetration_value[6];
			RJ=-0.60;
		}
		else if (dist>36 && dist<=42)
		{
			_to_hit=source->to_hit[7];
			_penetration_value=source->penetration_value[7];
			RJ=-0.65;
		}
		else if (dist>42 && dist<=48)
		{
			_to_hit=source->to_hit[8];
			_penetration_value=source->penetration_value[8];
			RJ=-0.70;
		}

		//firing unit modifiers
		//****************************
		//buttoned and has moved
		die_roll=die_roll+6;
		//acquisition
		die_roll=die_roll-2;
		//suppressed
		die_roll=die_roll+3*source->suppressed;
		//leader ability
		die_roll=die_roll+source->leader_ability;

		break;

	case 1:						//Heavy Weapons Squad

		X3=0;
		X4=0;
		_penetration_value=source->penetration_value[0];
		_to_hit=source->to_hit[0];
		RJ=-0.5;

		break;

	case 2:						//Marder IIIG

		X3=-0.2;						//firer moving slowly
		X4=0;							//AP round
		if (dist>0 && dist<=2)
		{
			_to_hit=source->to_hit[0];
			_penetration_value=source->penetration_value[0];
			RJ=-0.05;
		}
		else if (dist>2 && dist<=6)
		{
			_to_hit=source->to_hit[1];
			_penetration_value=source->penetration_value[1];
			RJ=-0.1;
		}
		else if (dist>6 && dist<=12)
		{
			_to_hit=source->to_hit[2];
			_penetration_value=source->penetration_value[2];
			RJ=-0.2;
		}
		else if (dist>12 && dist<=18)
		{
			_to_hit=source->to_hit[3];
			_penetration_value=source->penetration_value[3];
			RJ=-0.4;
		}
		else if (dist>18 && dist<=24)
		{
			_to_hit=source->to_hit[4];
			_penetration_value=source->penetration_value[4];
			RJ=-0.5;
		}
		else if (dist>24 && dist<=30)
		{
			_to_hit=source->to_hit[5];
			_penetration_value=source->penetration_value[5];
			RJ=-0.55;
		}
		else if (dist>30 && dist<=36)
		{
			_to_hit=source->to_hit[6];
			_penetration_value=source->penetration_value[6];
			RJ=-0.60;
		}
		else if (dist>36 && dist<=42)
		{
			_to_hit=source->to_hit[7];
			_penetration_value=source->penetration_value[7];
			RJ=-0.65;
		}
		else if (dist>42 && dist<=48)
		{
			_to_hit=source->to_hit[8];
			_penetration_value=source->penetration_value[8];
			RJ=-0.70;
		}

		//firing unit modifiers
		//****************************
		//has moved
		die_roll=die_roll+3;
		//acquisition
		die_roll=die_roll-2;
		//suppressed
		die_roll=die_roll+3*source->suppressed;
		//leader ability
		die_roll=die_roll+source->leader_ability;

		break;

	case 3:						//8cm Mortar Team

		X3=0;						//firer is stationary
		X4=0.2;						//HE round
		_penetration_value=9;

		if (dist>0 && dist<=1)
		{
			_to_hit=source->to_hit[0];
			RJ=-0.05;
		}
		else if (dist>1 && dist<=5)
		{
			_to_hit=source->to_hit[1];
			RJ=-0.1;
		}
		else if (dist>5 && dist<=10)
		{
			_to_hit=source->to_hit[2];
			RJ=-0.2;
		}
		else if (dist>10 && dist<=20)
		{
			_to_hit=source->to_hit[3];
			RJ=-0.4;
		}
		else if (dist>20 && dist<=35)
		{
			_to_hit=source->to_hit[4];
			RJ=-0.5;
		}
		else if (dist>35 && dist<=50)
		{
			_to_hit=source->to_hit[5];
			RJ=-0.75;
		}
		else if (dist>51 && dist<=60)
		{
			_to_hit=source->to_hit[6];
			RJ=-0.80;
		}

		//firing unit modifiers
		//****************************
		//acquisition
		die_roll=die_roll+2;
		//suppressed
		die_roll=die_roll+3*source->suppressed;
		//leader ability
		die_roll=die_roll+source->leader_ability;

		break;
	}

	//target unit modifiers
	//****************************
	//infantry target
	//if (target->object_id==3)
	//	die_roll=die_roll+3;
	//target moving
	die_roll=die_roll+2;
	//in LOS<=100m
	if (dist<=2)
		die_roll=die_roll+3;

	//hit chance percentage
	if (target->object_id==0)		//T-34 family
		X1=0;
	else if (target->object_id==1)	//T-70 tank
		X1=-0.2;					
	else if (target->object_id==3)	//Infantry
		X1=0.1;
	X2=-0.1;						//target moving slowly
	X5=0.1;							//firer has rangefinder

	hit_probability=(0.8+RJ+X1+X2+X3+X4+X5) * 100;
	app->scenario_interface->message3_hit_chance=CL_String((int)hit_probability);

	//check if we hit the target
	//std::cout << die_roll << ":" << _to_hit << std::endl;
	if (die_roll<=_to_hit)// && die_roll!=20)
	{
		//has been hit
		app->scenario_interface->message4_hit_results=CL_String("Target has been hit!");

		if (source->shot_coax>0 && source->shot_maingun==0 && (target->object_id==0 ||
			target->object_id==1))
		{
			app->scenario_interface->message6_hit_results=CL_String("No effect!");
		}

		if ((target->object_id==0 || target->object_id==1) &&
			source->shot_maingun>0)								//calculate the hit location, if it's a tank
		{
			//1D20 and 1D6 die_roll
			app->random_number.RandomInit(time(0));
			app->random_number.SetInterval(1,20);
			die_roll=app->random_number.iRandom();
			app->random_number.SetInterval(1,6);
			die_roll_six=app->random_number.iRandom();

			//hit location
			if (source->x==target->x && source->y < target->y)
			{
				if (target->hull==target->hull_array[5] || target->hull==target->hull_array[7])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[4] || target->hull==target->hull_array[0])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[2])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[6])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[1] || target->hull==target->hull_array[3])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x > target->x && source->y < target->y)
			{
				if (target->hull==target->hull_array[6] || target->hull==target->hull_array[0])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[1] || target->hull==target->hull_array[5])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[3])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[7])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[4] || target->hull==target->hull_array[2])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x > target->x && source->y == target->y)
			{
				if (target->hull==target->hull_array[1] || target->hull==target->hull_array[7])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[6] || target->hull==target->hull_array[2])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[4])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[0])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[3] || target->hull==target->hull_array[5])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x < target->x && source->y < target->y)
			{
				if (target->hull==target->hull_array[0] || target->hull==target->hull_array[2])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[7] || target->hull==target->hull_array[3])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[5])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[1])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[6] || target->hull==target->hull_array[4])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x == target->x && source->y > target->y)
			{
				if (target->hull==target->hull_array[1] || target->hull==target->hull_array[3])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[0] || target->hull==target->hull_array[4])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[6])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[2])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[5] || target->hull==target->hull_array[7])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x > target->x && source->y > target->y)
			{
				if (target->hull==target->hull_array[4] || target->hull==target->hull_array[2])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[1] || target->hull==target->hull_array[5])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[7])
				{
					location_variable=5;	//rear
				}
				else if (target->hull==target->hull_array[3])
				{
					location_variable=1;	//front
				}
				else if (target->hull==target->hull_array[6] || target->hull==target->hull_array[0])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x < target->x && source->y == target->y)
			{
				if (target->hull==target->hull_array[3] || target->hull==target->hull_array[5])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[6] || target->hull==target->hull_array[2])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[0])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[4])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[1] || target->hull==target->hull_array[7])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x < target->x && source->y > target->y)
			{
				if (target->hull==target->hull_array[4] || target->hull==target->hull_array[6])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[7] || target->hull==target->hull_array[3])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[1])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[5])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[0] || target->hull==target->hull_array[2])
				{
					location_variable=5;	//rear-side
				}
			}

			switch (location_variable)
			{
			case 0:		//side
				if (die_roll>=1 && die_roll<=9)
				{
					hit_location="TS";						//turret-side
					if (target->object_id==1)
						armor_value=7;
					else if (target->object_id==0)
						armor_value=15;
				}
				else if (die_roll>=10 && die_roll<=19)
				{
					hit_location="HS";						//hull-side
					if (target->object_id==1)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=20;
				}
				else if (die_roll==20)
				{
					hit_location="TK";						//track
				}

				break;

			case 1:		//front-side
				if (die_roll>=1 && die_roll<=4)
				{
					hit_location="TF";						//turret-front
					if (target->object_id==1)
						armor_value=9;
					else if (target->object_id==0)
						armor_value=36;
				}
				else if (die_roll>=5 && die_roll<=9)
				{
					hit_location="HF";						//hull-front
					if (target->object_id==1)
						armor_value=8;
					else if (target->object_id==0)
						armor_value=27;
				}
				else if (die_roll>=10 && die_roll<=13)
				{
					hit_location="TS";						//turret-side
					if (target->object_id==1)
						armor_value=7;
					else if (target->object_id==0)
						armor_value=15;
				}
				else if (die_roll>=14 && die_roll<=15)
				{
					hit_location="HS";						//hull-side
					if (target->object_id==1)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=20;
				}
				else if (die_roll>=16 && die_roll<=19)
				{
					hit_location="HR";						//hull-rear
					if (target->object_id==1)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=16;
				}
				else if (die_roll==20)
				{
					hit_location="TK";						//track
				}

				break;

			case 2:		//front
				if (die_roll>=1 && die_roll<=8)
				{
					hit_location="TF";						//turret-front
					if (target->object_id==1)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=25;
				}
				else if (die_roll>=9 && die_roll<=19)
				{
					hit_location="HF";						//hull-front
					if (target->object_id==1)
						armor_value=8;
					else if (target->object_id==0)
						armor_value=19;
				}
				else if (die_roll==20)
				{
					hit_location="TK";						//track
				}

				break;

			case 5:		//rear-side
				if (die_roll>=1 && die_roll<=4)
				{
					hit_location="TS";						//turret-side
					if (target->object_id==1)
						armor_value=7;
					else if (target->object_id==0)
						armor_value=15;
				}
				else if (die_roll>=5 && die_roll<=11)
				{
					hit_location="HS";						//hull-side
					if (target->object_id==1)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=20;
				}
				else if (die_roll>=12 && die_roll<=15)
				{
					hit_location="TR";						//turret-rear
					if (target->object_id==1)
						armor_value=4;
					else if (target->object_id==0)
						armor_value=13;
				}
				else if (die_roll>=16 && die_roll<=19)
				{
					hit_location="HR";						//hull-rear
					if (target->object_id==1)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=16;
				}
				else if (die_roll==20)
				{
					hit_location="TK";						//track
				}

				break;

			case 6:		//rear
				if (die_roll>=1 && die_roll<=9)
				{
					hit_location="TR";						//turret-rear
					if (target->object_id==1)
						armor_value=4;
					else if (target->object_id==0)
						armor_value=13;
				}
				else if (die_roll>=10 && die_roll<=19)
				{
					hit_location="HR";						//hull-rear
					if (target->object_id==1)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=16;
				}
				else if (die_roll==20)
				{
					hit_location="TK";						//track
				}

				break;
			}

			//check penetration
			if (_penetration_value>armor_value)
			{
				//penetrated
				app->scenario_interface->message5_hit_results=CL_String("The shot penetrated!");
				if (die_roll_six==1)
				{
					app->scenario_interface->message6_hit_results=CL_String("Burning!");
					target->is_dead=true;
					target->object_status=6;			//Burning
					target->is_burning=true;
					target->shot_coax=0;
					target->shot_maingun=0;
					target->movement_point=0;
					target->blt_priority=1;
					target->move0->stop();
					target->burning_frame_delay=CL_System::get_time();
				}
				else if (die_roll_six==2)
				{
					app->scenario_interface->message6_hit_results=CL_String("Wrecked!");

					target->is_dead=true;
					target->object_status=5;			//Wreck
					target->is_burning=true;
					target->shot_coax=0;
					target->shot_maingun=0;
					target->movement_point=0;
					target->blt_priority=1;
					target->move0->stop();
					target->burning_frame_delay=CL_System::get_time();

					//action check
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					//if fails the crew is dead otherwise bailed out
					if (die_roll<=target->morale)
					{
						app->number_of_russians++;
						app->objects.push_back(new GameObject_Russian(target->x+60,target->y,4,app->number_of_russians,app));
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+84)/TILE_WIDTH]='#';
					}

				}
				else if (die_roll_six>=3 && die_roll_six<=5)
				{
					app->scenario_interface->message6_hit_results=CL_String("Compartment damaged!");
					//both guns are damaged
					target->shot_coax=0;
					target->shot_maingun=0;
					target->suppressed=target->suppressed+2;

					//action check
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					//if fails the crew bails out
					if (die_roll>target->morale)
					{
						target->is_dead=true;
						app->scenario_interface->message6_hit_results=CL_String("Crew is bailing out!");
						target->object_status=4;			//abandoned
						target->movement_point=0;
						target->move0->stop();
						app->number_of_russians++;
						app->objects.push_back(new GameObject_Russian(target->x+60,target->y,4,app->number_of_russians,app));
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+84)/TILE_WIDTH]='#';
					}
				}
				else
				{
					app->scenario_interface->message6_hit_results=CL_String("No effect!");
					//action check
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					//if fails increase supression
					if (die_roll>target->morale)
						target->suppressed++;
				}
			}
			else
			{
				app->scenario_interface->message5_hit_results=CL_String("Shot bounched off!");

				app->random_number.RandomInit(time(0));
				app->random_number.SetInterval(1,12);
				die_roll=app->random_number.iRandom();

				if (die_roll>target->morale)
					target->suppressed=target->suppressed+1;
				
				//check for collateral damage
				//1D20 die roll
				app->random_number.RandomInit(time(0));
				app->random_number.SetInterval(1,20);
				die_roll=app->random_number.iRandom();

				if (hit_location=="HF") 
				{
					if (die_roll>=1 && die_roll<=3)
					{
						app->scenario_interface->message6_hit_results=CL_String("Bow Machine Gun is damaged!");
						target->shot_coax=0;
					}
					else if (die_roll>=6 && die_roll<=7)
					{
						app->scenario_interface->message6_hit_results=CL_String("Driver is dead!");
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
						target->suppressed=target->suppressed+1;
					}
					else if (die_roll>=14 && die_roll<=15)
					{
						app->scenario_interface->message6_hit_results=CL_String("Track is damaged!");
						target->object_status=7;		//Immobilized
						target->move0->stop();
						target->movement_point=0;
					}
					else if (die_roll>=18 && die_roll<=20)
					{
						app->scenario_interface->message6_hit_results=CL_String("Secondary damage!");
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
				if (hit_location=="HS")
				{
					if (die_roll>=1 && die_roll<=3)
					{
						app->scenario_interface->message6_hit_results=CL_String("Bogey is damaged!");
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
					}
					else if (die_roll>=6 && die_roll<=5)
					{
						app->scenario_interface->message6_hit_results=CL_String("Antenna is damaged!");
					}
					else if (die_roll>=11 && die_roll<=13)
					{
						app->scenario_interface->message6_hit_results=CL_String("Driver is dead!");
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
						target->suppressed=target->suppressed+1;
					}
					else if (die_roll>=18 && die_roll<=20)
					{
						app->scenario_interface->message6_hit_results=CL_String("Smoke discharger!");
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
				if (hit_location=="HR")
				{
					if (die_roll>=4 && die_roll<=5)
					{
						app->scenario_interface->message6_hit_results=CL_String("Engine is damaged!");
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
					}
					else if (die_roll>=8 && die_roll<=10)
					{
						app->scenario_interface->message6_hit_results=CL_String("Track is damaged!");
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
					}
					else if (die_roll>=14 && die_roll<=15)
					{
						app->scenario_interface->message6_hit_results=CL_String("Smoke discharger!");
					}
					else if (die_roll>=16 && die_roll<=17)
					{
						app->scenario_interface->message6_hit_results=CL_String("Fire!");
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
				if (hit_location=="TF")
				{
					if (die_roll>=1 && die_roll<=3)
					{
						app->scenario_interface->message6_hit_results=CL_String("Coax Machine Gun is damaged!");
						target->shot_coax=0;
					}
					else if (die_roll>=11 && die_roll<=13)
					{
						app->scenario_interface->message6_hit_results=CL_String("AA Machine Gun is damaged!");
					}
					else if (die_roll>=14 && die_roll<=15)
					{
						app->scenario_interface->message6_hit_results=CL_String("Commander is dead!");
						target->suppressed=target->suppressed+1;
					}
					else if (die_roll>=18 && die_roll<=20)
					{
						app->scenario_interface->message6_hit_results=CL_String("Optic is damaged!");
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
				if (hit_location=="TS")
				{
					if (die_roll>=1 && die_roll<=3)
					{
						app->scenario_interface->message6_hit_results=CL_String("AA Machine Gun is damaged!");
					}
					else if (die_roll>=4 && die_roll<=5)
					{
						app->scenario_interface->message6_hit_results=CL_String("Commander is dead!");
						target->suppressed=target->suppressed+1;
					}
					else if (die_roll>=8 && die_roll<=10)
					{
						app->scenario_interface->message6_hit_results=CL_String("Radio is damaged!");
					}
					else if (die_roll>=16 && die_roll<=17)
					{
						app->scenario_interface->message6_hit_results=CL_String("Turret ring is damaged!");
						target->shot_maingun=0;
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
				if (hit_location=="TR")
				{
					if (die_roll>=1 && die_roll<=3)
					{
						app->scenario_interface->message6_hit_results=CL_String("Turret ring is damaged!");
						target->shot_maingun=0;
					}
					else if (die_roll>=6 && die_roll<=7)
					{
						app->scenario_interface->message6_hit_results=CL_String("Rear Machine Gun is damaged!");
					}
					else if (die_roll>=14 && die_roll<=15)
					{
						app->scenario_interface->message6_hit_results=CL_String("AA Machine Gun is damaged!");
					}
					else if (die_roll>=18 && die_roll<=20)
					{
						app->scenario_interface->message6_hit_results=CL_String("Commander is dead!");
						target->suppressed=target->suppressed+1;
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
			}
		}
		else												//the target is infantry
		{
			/*if (source->shot_coax>0 && target->object_id==0 ||
				target->object_id==1)
			{
				app->scenario_interface->message6_hit_results=CL_String("No effect!");
			}*/

			if ((source->object_id==0 || source->object_id==3 || source->object_id==2) &&
				(target->object_id==3 || target->object_id==2 || target->object_id==4) && 
				source->shot_maingun>0)	
			{
				if (source->object_id==0 || source->object_id==2)
					_penetration_value=9;		//HE round for Pz.IVJ or Marder IIIG
				else if (source->object_id==3) 
					_penetration_value=9;		//8cm mortar round
			
				//2D6 die roll
				app->random_number.RandomInit(time(0));
				app->random_number.SetInterval(1,12);
				die_roll_six=app->random_number.iRandom();

				fire_power=_penetration_value+die_roll_six;
				//std::cout << fire_power << std::endl;

				if (fire_power>=1 && fire_power<=7)
				{
					app->scenario_interface->message6_hit_results=CL_String("No effect!");
				}
				else if (fire_power>=8 && fire_power<=9)
				{
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					if (die_roll>target->morale)
					{
						target->object_status=2;		//pinned
						target->movement_point=0;
						target->move0->stop();
						app->scenario_interface->message6_hit_results=CL_String("Enemy is Pinned!");
					}
				}
				else if (fire_power>=10 && fire_power<=11)
				{
					app->scenario_interface->message6_hit_results=CL_String("Enemy is Pinned!");
					target->object_status=2;			//pinned
					target->movement_point=0;
					target->move0->stop();
				}
				else if (fire_power>=12 && fire_power<=13)
				{
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					if (die_roll>target->morale)
					{
						app->scenario_interface->message6_hit_results=CL_String("Withdrawing!");
						target->object_status=3;		//withdrawing
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
						if (source->x > target->x)
							target->x-=48;
						else if (source->x < target->x)
							target->x+=48;
						if (source->y > target->y)
							target->y-=48;
						else if (source->y < target->y)
							target->y+=48;
						if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
							target->x=target->x+48;
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
						//target->hull=target->hull_array[0];
					}
				}
				else if (fire_power>=14 && fire_power<=16)
				{
					target->suppressed++;

					//if (target->object_id!=0 || target->object_id!=1)
					//{
						app->random_number.RandomInit(time(0));
						app->random_number.SetInterval(1,4);

						app->scenario_interface->message6_hit_results=CL_String("One dead!");
						app->rdeadsoldier_number++;
						target->number_of_soldiers=target->number_of_soldiers-1;
						app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
						app->random_number.SetInterval(1,60);
						app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
						app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();
						
						switch (target->number_of_soldiers)
						{
						case 9:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
							break;
						case 8:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
							break;
						case 7:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
							break;
						case 6:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
							break;
						case 5:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
							break;
						case 4:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
							break;
						case 3:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
							break;
						case 2:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
							break;
						case 1:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
							break;
						}

						if (target->number_of_soldiers<=0)
						{
							target->is_dead=true;
							target->is_visible=false;
						}
					//}
					//else
					//	target->suppressed+=2;
				}
				else if (fire_power>=17 && fire_power<=18)
				{
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					if (die_roll>target->morale)
					{
						target->do_astar=false;
						target->object_status=8;		//routed
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
						if (source->x > target->x)
							target->x-=96;
						else if (source->x < target->x)
							target->x+=96;
						if (source->y > target->y)
							target->y-=96;
						else if (source->y < target->y)
							target->y+=96;
						if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
							target->x=target->x+48;
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
						//target->hull=target->hull_array[0];
						target->shot_coax=0;
						target->shot_maingun=0;
						target->move0->stop();
					}
				}
				else if (fire_power==19)
				{
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					if (die_roll>target->morale)
					{
						target->suppressed++;

						//if (target->object_id!=0 || target->object_id!=1)
						//{
							app->random_number.RandomInit(time(0));
							app->random_number.SetInterval(1,4);

							app->scenario_interface->message6_hit_results=CL_String("One dead!");
							app->rdeadsoldier_number++;
							target->number_of_soldiers=target->number_of_soldiers-1;
							app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
							app->random_number.SetInterval(1,60);
							app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
							app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();

							switch (target->number_of_soldiers)
							{
							case 9:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
								break;
							case 8:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
								break;
							case 7:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
								break;
							case 6:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
								break;
							case 5:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
								break;
							case 4:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
								break;
							case 3:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
								break;
							case 2:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
								break;
							case 1:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
								break;
							}

							if (target->number_of_soldiers<=0)
							{
								target->is_dead=true;
								target->is_visible=false;
							}
						//}
						//else
						//	target->suppressed+=2;
					}
				}
				else if (fire_power==20)
				{
					target->suppressed+=2;

					//if (target->object_id!=0 || target->object_id!=1)
					//{
						app->random_number.RandomInit(time(0));

						app->scenario_interface->message6_hit_results=CL_String("Two dead!");

						for (int j=0;j<2;j++)
						{
							app->rdeadsoldier_number++;
							app->random_number.SetInterval(1,4);
							app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
							app->random_number.SetInterval(1,60);
							app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
							app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();
						}
						
						target->number_of_soldiers=target->number_of_soldiers-2;
					
						switch (target->number_of_soldiers)
						{
						case 9:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
							break;
						case 8:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
							break;
						case 7:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
							break;
						case 6:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
							break;
						case 5:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
							break;
						case 4:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
							break;
						case 3:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
							break;
						case 2:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
							break;
						case 1:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
							break;
						}

						if (target->number_of_soldiers<=0)
						{
							target->is_dead=true;
							target->is_visible=false;
						}
					//}
				}
				else if (fire_power>20)
				{
					target->suppressed+=3;

					//if (target->object_id!=0 || target->object_id!=1)
					//{
						app->random_number.RandomInit(time(0));

						app->scenario_interface->message6_hit_results=CL_String("Three are killed!");

						for (int j=0;j<3;j++)
						{
							app->rdeadsoldier_number++;
							app->random_number.SetInterval(1,4);
							app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
							app->random_number.SetInterval(1,60);
							app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
							app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();
						}

						target->number_of_soldiers=target->number_of_soldiers-3;
						
						switch (target->number_of_soldiers)
						{
						case 9:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
							break;
						case 8:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
							break;
						case 7:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
							break;
						case 6:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
							break;
						case 5:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
							break;
						case 4:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
							break;
						case 3:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
							break;
						case 2:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
							break;
						case 1:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
							break;
						}

						if (target->number_of_soldiers<=0)
						{
							target->is_dead=true;
							target->is_visible=false;
						}
					//}
				}
			}
			else if ((source->object_id==0 || source->object_id==1 || source->object_id==2) && 
				(target->object_id==3 || target->object_id==2 || target->object_id==4) && 
				source->shot_coax>0)
			{
				if (source->object_id==0 || source->object_id==2)
					_penetration_value=4;		//Pz.IVJ CMG	Marder III CMG
				else if (source->object_id==1)
					_penetration_value=10-(2*(9-source->number_of_soldiers));		//infantry squad's fire power

				//2D6 die roll
				app->random_number.RandomInit(time(0));
				app->random_number.SetInterval(1,12);
				die_roll_six=app->random_number.iRandom();

				fire_power=_penetration_value+die_roll_six;

				if (fire_power>=1 && fire_power<=7)
				{
					app->scenario_interface->message6_hit_results=CL_String("No effect!");
				}
				else if (fire_power>=8 && fire_power<=9)
				{
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					if (die_roll>target->morale)
					{
						target->object_status=2;		//pinned
						target->movement_point=0;
						target->move0->stop();
						app->scenario_interface->message6_hit_results=CL_String("Enemy is Pinned!");
					}
				}
				else if (fire_power>=10 && fire_power<=11)
				{
					app->scenario_interface->message6_hit_results=CL_String("Enemy is Pinned!");
					target->object_status=2;			//pinned
					target->movement_point=0;
					target->move0->stop();
				}
				else if (fire_power>=12 && fire_power<=13)
				{
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					if (die_roll>target->morale)
					{
						app->scenario_interface->message6_hit_results=CL_String("Withdrawing!");
						target->object_status=3;		//withdrawing
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
						if (source->x > target->x)
							target->x-=48;
						else if (source->x < target->x)
							target->x+=48;
						if (source->y > target->y)
							target->y-=48;
						else if (source->y < target->y)
							target->y+=48;
						if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
							target->x=target->x+48;
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
						//target->hull=target->hull_array[0];
					}
				}
				else if (fire_power>=14 && fire_power<=16)
				{
					target->suppressed++;

					//if (target->object_id!=0 || target->object_id!=1)
					//{
						app->random_number.RandomInit(time(0));
						app->random_number.SetInterval(1,4);

						app->scenario_interface->message6_hit_results=CL_String("One dead!");
						app->rdeadsoldier_number++;
						target->number_of_soldiers=target->number_of_soldiers-1;
						app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
						app->random_number.SetInterval(1,60);
						app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
						app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();

						switch (target->number_of_soldiers)
						{
						case 9:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
							break;
						case 8:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
							break;
						case 7:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
							break;
						case 6:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
							break;
						case 5:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
							break;
						case 4:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
							break;
						case 3:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
							break;
						case 2:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
							break;
						case 1:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
							break;
						}

						if (target->number_of_soldiers<=0)
						{
							target->is_dead=true;
							target->is_visible=false;
						}
					//}
					//else
					//	target->suppressed+=2;
				}
				else if (fire_power>=17 && fire_power<=18)
				{
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					if (die_roll>target->morale)
					{
						target->do_astar=false;
						target->object_status=8;		//routed
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
						if (source->x > target->x)
							target->x-=96;
						else if (source->x < target->x)
							target->x+=96;
						if (source->y > target->y)
							target->y-=96;
						else if (source->y < target->y)
							target->y+=96;
						if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
							target->x=target->x+48;
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
						//target->hull=target->hull_array[0];
						target->shot_coax=0;
						target->shot_maingun=0;
						target->move0->stop();
					}
				}
				else if (fire_power==19)
				{
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,12);
					die_roll=app->random_number.iRandom();

					if (die_roll>target->morale)
					{
						target->suppressed++;

						//if (target->object_id!=0 || target->object_id!=1)
						//{
							app->random_number.RandomInit(time(0));
							app->random_number.SetInterval(1,4);

							app->scenario_interface->message6_hit_results=CL_String("One dead!");
							app->rdeadsoldier_number++;
							target->number_of_soldiers=target->number_of_soldiers-1;
							app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
							app->random_number.SetInterval(1,60);
							app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
							app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();
							switch (target->number_of_soldiers)
							{
							case 9:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
								break;
							case 8:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
								break;
							case 7:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
								break;
							case 6:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
								break;
							case 5:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
								break;
							case 4:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
								break;
							case 3:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
								break;
							case 2:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
								break;
							case 1:
								target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
								target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
								break;
							}

							if (target->number_of_soldiers<=0)
							{
								target->is_dead=true;
								target->is_visible=false;
							}
						//}
						//else
						//	target->suppressed+=2;
					}
				}
				else if (fire_power==20)
				{
					target->suppressed+=2;

					//if (target->object_id!=0 || target->object_id!=1)
					//{
						app->random_number.RandomInit(time(0));

						app->scenario_interface->message6_hit_results=CL_String("Two dead!");
						for (int j=0;j<2;j++)
						{
							app->rdeadsoldier_number++;
							app->random_number.SetInterval(1,4);
							app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
							app->random_number.SetInterval(1,60);
							app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
							app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();
						}
						
						target->number_of_soldiers=target->number_of_soldiers-2;
						
						switch (target->number_of_soldiers)
						{
						case 9:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
							break;
						case 8:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
							break;
						case 7:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
							break;
						case 6:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
							break;
						case 5:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
							break;
						case 4:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
							break;
						case 3:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
							break;
						case 2:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
							break;
						case 1:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
							break;
						}

						if (target->number_of_soldiers<=0)
						{
							target->is_dead=true;
							target->is_visible=false;
						}
					//}
				}
				else if (fire_power>20)
				{
					target->suppressed+=3;

					//if (target->object_id!=0 || target->object_id!=1)
					//{
						app->random_number.RandomInit(time(0));

						app->scenario_interface->message6_hit_results=CL_String("Three are killed!");
						for (int j=0;j<3;j++)
						{
							app->rdeadsoldier_number++;
							app->random_number.SetInterval(1,4);
							app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
							app->random_number.SetInterval(1,60);
							app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
							app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();
						}

						target->number_of_soldiers=target->number_of_soldiers-3;
						
						switch (target->number_of_soldiers)
						{
						case 9:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
							break;
						case 8:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
							break;
						case 7:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
							break;
						case 6:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
							break;
						case 5:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
							break;
						case 4:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
							break;
						case 3:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
							break;
						case 2:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
							break;
						case 1:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
							break;
						}

						if (target->number_of_soldiers<=0)
						{
							target->is_dead=true;
							target->is_visible=false;
						}
					//}
				}
			}
		}
	}
	//this is totally wrong needs rewrite!!!
	//****************************************
	//It missed...
	else
	{
		if (source->object_id==1)
		{
			//2D6 die roll
			app->random_number.RandomInit(time(0));
			app->random_number.SetInterval(1,12);
			die_roll_six=app->random_number.iRandom();

			if (source->object_id==1)
					_penetration_value=10-(2*(9-source->number_of_soldiers));		//infantry squad's fire power

			if (dist>=6 || source->object_status==2 || source->object_status==3)
				_penetration_value=_penetration_value/2;

			if (target->object_id==0 || target->object_id==1)		//T-34,T-70 tanks
				fire_power=_penetration_value+die_roll_six-4;
			else 
				fire_power=_penetration_value+die_roll_six;

			if (fire_power>=1 && fire_power<=7)
			{
				app->scenario_interface->message6_hit_results=CL_String("No effect!");
			}
			else if (fire_power>=8 && fire_power<=9)
			{
				app->random_number.RandomInit(time(0));
				app->random_number.SetInterval(1,12);
				die_roll=app->random_number.iRandom();

				if (die_roll>target->morale)
				{
					target->object_status=2;		//pinned
					target->movement_point=0;
					target->move0->stop();
					app->scenario_interface->message6_hit_results=CL_String("Enemy is Pinned!");
				}
			}
			else if (fire_power>=10 && fire_power<=11)
			{
				app->scenario_interface->message6_hit_results=CL_String("Enemy is Pinned!");
				target->object_status=2;			//pinned
				target->movement_point=0;
				target->move0->stop();
			}
			else if (fire_power>=12 && fire_power<=13)
			{
				app->random_number.RandomInit(time(0));
				app->random_number.SetInterval(1,12);
				die_roll=app->random_number.iRandom();

				if (die_roll>target->morale)
				{
					app->scenario_interface->message6_hit_results=CL_String("Withdrawing!");
					target->object_status=3;		//withdrawing
					app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
					if (source->x > target->x)
						target->x-=48;
					else if (source->x < target->x)
						target->x+=48;
					if (source->y > target->y)
						target->y-=48;
					else if (source->y < target->y)
						target->y+=48;
					if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
							target->x=target->x+48;
					app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
					//target->hull=target->hull_array[0];
				}
			}
			else if (fire_power>=14 && fire_power<=16)
			{
				if (target->object_id==2 || target->object_id==3 ||
					target->object_id==4)
				{
					app->random_number.RandomInit(time(0));
					app->random_number.SetInterval(1,4);

					app->scenario_interface->message6_hit_results=CL_String("One dead!");
					target->suppressed++;
					app->rdeadsoldier_number++;
					target->number_of_soldiers=target->number_of_soldiers-1;
					app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
					app->random_number.SetInterval(1,60);
					app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
					app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();

					switch (target->number_of_soldiers)
					{
					case 9:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
						break;
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
				else
					target->suppressed+=2;
			}
			else if (fire_power>=17 && fire_power<=18)
			{
				app->random_number.RandomInit(time(0));
				app->random_number.SetInterval(1,12);
				die_roll=app->random_number.iRandom();

				if (die_roll>target->morale)
				{
					target->do_astar=false;
					target->object_status=8;		//routed
					app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
					if (source->x > target->x)
						target->x-=96;
					else if (source->x < target->x)
						target->x+=96;
					if (source->y > target->y)
						target->y-=96;
					else if (source->y < target->y)
						target->y+=96;
					if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
						target->x=target->x+48;
					app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
					//target->hull=target->hull_array[0];
					target->shot_coax=0;
					target->shot_maingun=0;
					target->move0->stop();
				}
			}
			else if (fire_power==19)
			{
				app->random_number.RandomInit(time(0));
				app->random_number.SetInterval(1,12);
				die_roll=app->random_number.iRandom();

				if (die_roll>target->morale)
				{
					if (target->object_id==2 || target->object_id==3 ||
						target->object_id==4)
					{
						app->random_number.RandomInit(time(0));
						app->random_number.SetInterval(1,4);

						app->scenario_interface->message6_hit_results=CL_String("One dead!");
						target->suppressed++;
						app->rdeadsoldier_number++;
						target->number_of_soldiers=target->number_of_soldiers-1;
						app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
						app->random_number.SetInterval(1,60);
						app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
						app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();

						switch (target->number_of_soldiers)
						{
						case 9:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
							break;
						case 8:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
							break;
						case 7:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
							break;
						case 6:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
							break;
						case 5:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
							break;
						case 4:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
							break;
						case 3:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
							break;
						case 2:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
							break;
						case 1:
							target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
							break;
						}

						if (target->number_of_soldiers<=0)
						{
							target->is_dead=true;
							target->is_visible=false;
						}
					}
					else
						target->suppressed+=2;
				}
			}
			else if (fire_power==20)
			{
				target->suppressed+=2;

				if (target->object_id==2 || target->object_id==3 ||
					target->object_id==4)
				{
					app->random_number.RandomInit(time(0));

					app->scenario_interface->message6_hit_results=CL_String("Two dead!");
					for (int j=0;j<2;j++)
					{
						app->rdeadsoldier_number++;
						app->random_number.SetInterval(1,4);
						app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
						app->random_number.SetInterval(1,60);
						app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
						app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();
					}
					
					target->number_of_soldiers=target->number_of_soldiers-2;
					
					switch (target->number_of_soldiers)
					{
					case 9:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
						break;
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
			}
			else if (fire_power>20)
			{
				target->suppressed+=3;

				if (target->object_id==2 || target->object_id==3 ||
					target->object_id==4)
				{
					app->random_number.RandomInit(time(0));

					app->scenario_interface->message6_hit_results=CL_String("Three are killed!");
					for (int j=0;j<3;j++)
					{
						app->rdeadsoldier_number++;
						app->random_number.SetInterval(1,4);
						app->rdeadbody_frame[app->rdeadsoldier_number]=app->random_number.iRandom();
						app->random_number.SetInterval(1,60);
						app->rdeadsoldier_coordinates_x[app->rdeadsoldier_number]=target->x+app->random_number.iRandom();
						app->rdeadsoldier_coordinates_y[app->rdeadsoldier_number]=target->y+app->random_number.iRandom();
					}

					target->number_of_soldiers=target->number_of_soldiers-3;
					
					switch (target->number_of_soldiers)
					{
					case 9:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
						break;
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
			}
		}
		else
			app->scenario_interface->message4_hit_results=CL_String("It missed...");	
	}
}

//*****************************************************
void Analysis::r_target_eliminated(GameObject* source,GameObject* target,Kursk* app)
//*****************************************************
{
	int dist,_penetration_value,armor_value,fire_power,location_variable;
	float hit_probability,_to_hit,die_roll, die_roll_six;
	double X1,X2,X3,X4,X5;
	double RJ;
	char* hit_location;

	hit_location="";

	//the distance between two units in cells.
	dist = Math::distance(source->x,source->y,target->x,target->y);

	//1D20 die_roll
	srand((unsigned)time(NULL));
	die_roll=rand()%20+1;

	//gun malfunction
	//{
	//	source->object_status=1;

	//	if (source->shot_maingun > 0)
	//		source->shot_maingun=0;
	//	else if (source->shot_coax > 0)
	//		source->shot_coax=0;
	//}

	//to hit variable, depending on the target distance and the firing unit
	switch (source->object_id)
	{
	case 0:						//T-34

		X3=0;						//firer is stationary
		X4=0;							//AP round
		if (dist>0 && dist<=2)
		{
			_to_hit=source->to_hit[0];
			_penetration_value=source->penetration_value[0];
			RJ=-0.05;
		}
		else if (dist>2 && dist<=6)
		{
			_to_hit=source->to_hit[1];
			_penetration_value=source->penetration_value[1];
			RJ=-0.1;
		}
		else if (dist>6 && dist<=12)
		{
			_to_hit=source->to_hit[2];
			_penetration_value=source->penetration_value[2];
			RJ=-0.2;
		}
		else if (dist>12 && dist<=18)
		{
			_to_hit=source->to_hit[3];
			_penetration_value=source->penetration_value[3];
			RJ=-0.4;
		}
		else if (dist>18 && dist<=24)
		{
			_to_hit=source->to_hit[4];
			_penetration_value=source->penetration_value[4];
			RJ=-0.5;
		}
		else if (dist>24 && dist<=30)
		{
			_to_hit=source->to_hit[5];
			_penetration_value=source->penetration_value[5];
			RJ=-0.55;
		}
		else if (dist>30 && dist<=36)
		{
			_to_hit=source->to_hit[6];
			_penetration_value=source->penetration_value[6];
			RJ=-0.60;
		}
		else if (dist>36 && dist<=42)
		{
			_to_hit=source->to_hit[7];
			_penetration_value=source->penetration_value[7];
			RJ=-0.65;
		}
		else if (dist>42 && dist<=48)
		{
			_to_hit=source->to_hit[8];
			_penetration_value=source->penetration_value[8];
			RJ=-0.70;
		}

		//firing unit modifiers
		//****************************
		//buttoned and has moved
		die_roll=die_roll+3;
		//acquisition
		die_roll=die_roll-2;
		//suppressed
		die_roll=die_roll+3*source->suppressed;
		//leader ability
		die_roll=die_roll+source->leader_ability;

		break;

	case 3:						//Infantry Squad

		X3=0;
		X4=0;
		_penetration_value=source->penetration_value[0];
		_to_hit=source->to_hit[0];
		RJ=-0.5;

		break;

	case 1:						//T-70

		X3=0;							//firer moving slowly
		X4=0;							//AP round
		if (dist>0 && dist<=2)
		{
			_to_hit=source->to_hit[0];
			_penetration_value=source->penetration_value[0];
			RJ=-0.05;
		}
		else if (dist>2 && dist<=6)
		{
			_to_hit=source->to_hit[1];
			_penetration_value=source->penetration_value[1];
			RJ=-0.1;
		}
		else if (dist>6 && dist<=12)
		{
			_to_hit=source->to_hit[2];
			_penetration_value=source->penetration_value[2];
			RJ=-0.2;
		}
		else if (dist>12 && dist<=18)
		{
			_to_hit=source->to_hit[3];
			_penetration_value=source->penetration_value[3];
			RJ=-0.4;
		}
		else if (dist>18 && dist<=24)
		{
			_to_hit=source->to_hit[4];
			_penetration_value=source->penetration_value[4];
			RJ=-0.5;
		}
		else if (dist>24 && dist<=30)
		{
			_to_hit=source->to_hit[5];
			_penetration_value=source->penetration_value[5];
			RJ=-0.55;
		}
		else if (dist>30 && dist<=36)
		{
			_to_hit=source->to_hit[6];
			_penetration_value=source->penetration_value[6];
			RJ=-0.60;
		}
		else if (dist>36 && dist<=42)
		{
			_to_hit=source->to_hit[7];
			_penetration_value=source->penetration_value[7];
			RJ=-0.65;
		}
		else if (dist>42 && dist<=48)
		{
			_to_hit=source->to_hit[8];
			_penetration_value=source->penetration_value[8];
			RJ=-0.70;
		}

		//firing unit modifiers
		//****************************
		//has buttoned up
		die_roll=die_roll+3;
		//acquisition
		die_roll=die_roll-2;
		//suppressed
		die_roll=die_roll+3*source->suppressed;
		//leader ability
		die_roll=die_roll+source->leader_ability;

		break;

	case 2:						//120mm Mortar Team

		X3=0;						//firer is stationary
		X4=0.2;						//HE round
		_penetration_value=13;

		if (dist>0 && dist<=1)
		{
			_to_hit=source->to_hit[0];
			RJ=-0.05;
		}
		else if (dist>1 && dist<=5)
		{
			_to_hit=source->to_hit[1];
			RJ=-0.1;
		}
		else if (dist>5 && dist<=10)
		{
			_to_hit=source->to_hit[2];
			RJ=-0.2;
		}
		else if (dist>10 && dist<=20)
		{
			_to_hit=source->to_hit[3];
			RJ=-0.4;
		}
		else if (dist>20 && dist<=35)
		{
			_to_hit=source->to_hit[4];
			RJ=-0.5;
		}
		else if (dist>35 && dist<=50)
		{
			_to_hit=source->to_hit[5];
			RJ=-0.75;
		}
		else if (dist>51 && dist<=60)
		{
			_to_hit=source->to_hit[6];
			RJ=-0.80;
		}

		//firing unit modifiers
		//****************************
		//acquisition
		die_roll=die_roll+2;
		//suppressed
		die_roll=die_roll+3*source->suppressed;
		//leader ability
		die_roll=die_roll+source->leader_ability;

		break;
	}

	//target unit modifiers
	//****************************
	//infantry target
	if (target->object_id==1)
		die_roll=die_roll+3;
	//target moving
	die_roll=die_roll+2;
	//in LOS<=100m
	if (dist<=2)
		die_roll=die_roll+3;

	//hit chance percentage
	if (target->object_id==0)		//Pz.IV family
		X1=0;
	else if (target->object_id==2)	//Marder IIIG
		X1=-0.2;					
	else if (target->object_id==1)	//Infantry
		X1=0.1;
	X2=-0.1;						//target moving slowly
	X5=0.1;							//firer has rangefinder

	hit_probability=(0.8+RJ+X1+X2+X3+X4+X5) * 100;
	//app->scenario_interface->message3_hit_chance=CL_String((int)hit_probability);

	//check if we hit the target
	if (die_roll<=_to_hit && die_roll!=20)
	{
		//has been hit
		//app->scenario_interface->message4_hit_results=CL_String("Target has been hit!");

		if ((target->object_id==0 || target->object_id==2) && source->shot_maingun>0)		//calculate the hit location, if it's a tank
		{
			//1D20 and 1D6 die_roll
			srand((unsigned)time(NULL));
			die_roll=rand()%20;
			die_roll_six=rand()%6;

			//hit location
			if (source->x==target->x && source->y < target->y)
			{
				if (target->hull==target->hull_array[5] || target->hull==target->hull_array[7])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[4] || target->hull==target->hull_array[0])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[2])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[6])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[1] || target->hull==target->hull_array[3])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x > target->x && source->y < target->y)
			{
				if (target->hull==target->hull_array[6] || target->hull==target->hull_array[0])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[1] || target->hull==target->hull_array[5])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[3])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[7])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[4] || target->hull==target->hull_array[2])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x > target->x && source->y == target->y)
			{
				if (target->hull==target->hull_array[1] || target->hull==target->hull_array[7])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[6] || target->hull==target->hull_array[2])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[4])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[0])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[3] || target->hull==target->hull_array[5])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x < target->x && source->y < target->y)
			{
				if (target->hull==target->hull_array[0] || target->hull==target->hull_array[2])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[7] || target->hull==target->hull_array[3])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[5])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[1])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[6] || target->hull==target->hull_array[4])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x == target->x && source->y > target->y)
			{
				if (target->hull==target->hull_array[1] || target->hull==target->hull_array[3])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[0] || target->hull==target->hull_array[4])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[6])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[2])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[5] || target->hull==target->hull_array[7])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x > target->x && source->y > target->y)
			{
				if (target->hull==target->hull_array[4] || target->hull==target->hull_array[2])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[1] || target->hull==target->hull_array[5])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[7])
				{
					location_variable=5;	//rear
				}
				else if (target->hull==target->hull_array[3])
				{
					location_variable=1;	//front
				}
				else if (target->hull==target->hull_array[6] || target->hull==target->hull_array[0])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x < target->x && source->y == target->y)
			{
				if (target->hull==target->hull_array[3] || target->hull==target->hull_array[5])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[6] || target->hull==target->hull_array[2])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[0])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[4])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[1] || target->hull==target->hull_array[7])
				{
					location_variable=5;	//rear-side
				}
			}
			else if (source->x < target->x && source->y > target->y)
			{
				if (target->hull==target->hull_array[4] || target->hull==target->hull_array[6])
				{
					location_variable=1;	//front-side
				}
				else if (target->hull==target->hull_array[7] || target->hull==target->hull_array[3])
				{
					location_variable=0;	//side
				}
				else if (target->hull==target->hull_array[1])
				{
					location_variable=6;	//rear
				}
				else if (target->hull==target->hull_array[5])
				{
					location_variable=2;	//front
				}
				else if (target->hull==target->hull_array[0] || target->hull==target->hull_array[2])
				{
					location_variable=5;	//rear-side
				}
			}

			switch (location_variable)
			{
			case 0:		//side
				if (die_roll>=1 && die_roll<=9)
				{
					hit_location="TS";						//turret-side
					if (target->object_id==2)
						armor_value=7;
					else if (target->object_id==0)
						armor_value=13;
				}
				else if (die_roll>=10 && die_roll<=19)
				{
					hit_location="HS";						//hull-side
					if (target->object_id==2)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=8;
				}
				else if (die_roll==20)
				{
					hit_location="TK";						//track
				}

				break;

			case 1:		//front-side
				if (die_roll>=1 && die_roll<=4)
				{
					hit_location="TF";						//turret-front
					if (target->object_id==2)
						armor_value=9;
					else if (target->object_id==0)
						armor_value=17;
				}
				else if (die_roll>=5 && die_roll<=9)
				{
					hit_location="HF";						//hull-front
					if (target->object_id==2)
						armor_value=8;
					else if (target->object_id==0)
						armor_value=17;
				}
				else if (die_roll>=10 && die_roll<=13)
				{
					hit_location="TS";						//turret-side
					if (target->object_id==2)
						armor_value=7;
					else if (target->object_id==0)
						armor_value=13;
				}
				else if (die_roll>=14 && die_roll<=15)
				{
					hit_location="HS";						//hull-side
					if (target->object_id==2)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=8;
				}
				else if (die_roll>=16 && die_roll<=19)
				{
					hit_location="HR";						//hull-rear
					if (target->object_id==2)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=9;
				}
				else if (die_roll==20)
				{
					hit_location="TK";						//track
				}

				break;

			case 2:		//front
				if (die_roll>=1 && die_roll<=8)
				{
					hit_location="TF";						//turret-front
					if (target->object_id==2)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=17;
				}
				else if (die_roll>=9 && die_roll<=19)
				{
					hit_location="HF";						//hull-front
					if (target->object_id==2)
						armor_value=8;
					else if (target->object_id==0)
						armor_value=17;
				}
				else if (die_roll==20)
				{
					hit_location="TK";						//track
				}

				break;

			case 5:		//rear-side
				if (die_roll>=1 && die_roll<=4)
				{
					hit_location="TS";						//turret-side
					if (target->object_id==2)
						armor_value=7;
					else if (target->object_id==0)
						armor_value=13;
				}
				else if (die_roll>=5 && die_roll<=11)
				{
					hit_location="HS";						//hull-side
					if (target->object_id==2)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=8;
				}
				else if (die_roll>=12 && die_roll<=15)
				{
					hit_location="TR";						//turret-rear
					if (target->object_id==2)
						armor_value=4;
					else if (target->object_id==0)
						armor_value=10;
				}
				else if (die_roll>=16 && die_roll<=19)
				{
					hit_location="HR";						//hull-rear
					if (target->object_id==2)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=9;
				}
				else if (die_roll==20)
				{
					hit_location="TK";						//track
				}

				break;

			case 6:		//rear
				if (die_roll>=1 && die_roll<=9)
				{
					hit_location="TR";						//turret-rear
					if (target->object_id==2)
						armor_value=4;
					else if (target->object_id==0)
						armor_value=10;
				}
				else if (die_roll>=10 && die_roll<=19)
				{
					hit_location="HR";						//hull-rear
					if (target->object_id==2)
						armor_value=6;
					else if (target->object_id==0)
						armor_value=9;
				}
				else if (die_roll==20)
				{
					hit_location="TK";						//track
				}

				break;
			}

			//check penetration
			if (_penetration_value>armor_value)
			{
				//penetrated
				if (die_roll_six==1)
				{
					target->is_dead=true;
					target->object_status=6;			//Burning
					target->is_burning=true;
					target->shot_coax=0;
					target->shot_maingun=0;
					target->movement_point=0;
					target->move0->stop();
					target->blt_priority=1;
					target->burning_frame_delay=CL_System::get_time();
					target->enemy_number=0;
					target->suppressed=0;
				}
				else if (die_roll_six==2)
				{
					target->is_dead=true;
					target->object_status=5;			//Wreck
					target->is_burning=true;
					target->shot_coax=0;
					target->shot_maingun=0;
					target->movement_point=0;
					target->move0->stop();
					target->blt_priority=1;
					target->burning_frame_delay=CL_System::get_time();
					target->enemy_number=0;
					target->suppressed=0;

					//action check
					srand((unsigned)time(NULL));
					die_roll=rand()%12;
					//if fails the crew is dead otherwise bailed out
					if (die_roll<=target->morale)
					{
						app->number_of_germans++;				
						app->objects.push_back(new GameObject_German(target->x-60,target->y,4,app->number_of_germans,app));
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x-84)/TILE_WIDTH]='#';
					}
				}
				else if (die_roll_six>=3 && die_roll_six<=5)
				{
					//both guns are damaged
					target->shot_coax=0;
					target->shot_maingun=0;
					target->object_status=1;
					target->suppressed=target->suppressed+2;

					//action check
					srand((unsigned)time(NULL));
					die_roll=rand()%12;
					//if fails the crew bails out
					if (die_roll>target->morale)
					{		
						app->number_of_germans++;
						app->objects.push_back(new GameObject_German(target->x-60,target->y,4,app->number_of_germans,app));
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x-84)/TILE_WIDTH]='#';
						target->is_dead=true;
						target->object_status=4;			//abandoned
						target->movement_point=0;
						target->move0->stop();
						target->enemy_number=0;
						target->suppressed=0;
					}
				}
				else
				{
					srand((unsigned)time(NULL));
					die_roll=rand()%12;
					//if fails increase supression
					if (die_roll>target->morale)
					{
						target->suppressed++;
					}
				}
			}
			else
			{
				app->scenario_interface->message5_hit_results=CL_String("Shot bounched off!");
				srand((unsigned)time(NULL));
				//2D6 die roll
				die_roll=rand()%12;
				if (die_roll>target->morale)
				{
					target->suppressed=target->suppressed+1;
				}				
				//check for collateral damage
				//1D20 die roll
				die_roll=rand()%20;
				if (hit_location=="HF") 
				{
					if (die_roll>=1 && die_roll<=3)
					{
						target->shot_coax=0;
					}
					else if (die_roll>=6 && die_roll<=7)
					{
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
						target->suppressed=target->suppressed+1;
					}
					else if (die_roll>=14 && die_roll<=15)
					{
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
					}
					else if (die_roll>=18 && die_roll<=20)
					{
						app->scenario_interface->message6_hit_results=CL_String("Secondary damage!");
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
				if (hit_location=="HS")
				{
					if (die_roll>=1 && die_roll<=3)
					{
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
					}
					else if (die_roll>=6 && die_roll<=5)
					{
						app->scenario_interface->message6_hit_results=CL_String("Antenna is damaged!");
					}
					else if (die_roll>=11 && die_roll<=13)
					{
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
						target->suppressed=target->suppressed+1;
					}
					else if (die_roll>=18 && die_roll<=20)
					{
						app->scenario_interface->message6_hit_results=CL_String("Smoke discharger!");
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
				if (hit_location=="HR")
				{
					if (die_roll>=4 && die_roll<=5)
					{
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
					}
					else if (die_roll>=8 && die_roll<=10)
					{			
						target->object_status=7;		//Immobilized
						target->movement_point=0;
						target->move0->stop();
					}
					else if (die_roll>=14 && die_roll<=15)
					{
						app->scenario_interface->message6_hit_results=CL_String("Smoke discharger!");
					}
					else if (die_roll>=16 && die_roll<=17)
					{
						app->scenario_interface->message6_hit_results=CL_String("Fire!");
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
				if (hit_location=="TF")
				{
					if (die_roll>=1 && die_roll<=3)
					{
						target->shot_coax=0;
						target->object_status=1;
					}
					else if (die_roll>=11 && die_roll<=13)
					{
						app->scenario_interface->message6_hit_results=CL_String("AA Machine Gun is damaged!");
					}
					else if (die_roll>=14 && die_roll<=15)
					{						
						target->suppressed=target->suppressed+1;				
					}
					else if (die_roll>=18 && die_roll<=20)
					{
						app->scenario_interface->message6_hit_results=CL_String("Optic is damaged!");
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
				if (hit_location=="TS")
				{
					if (die_roll>=1 && die_roll<=3)
					{
						app->scenario_interface->message6_hit_results=CL_String("AA Machine Gun is damaged!");
					}
					else if (die_roll>=4 && die_roll<=5)
					{						
						target->suppressed=target->suppressed+1;					
					}
					else if (die_roll>=8 && die_roll<=10)
					{
						app->scenario_interface->message6_hit_results=CL_String("Radio is damaged!");
					}
					else if (die_roll>=16 && die_roll<=17)
					{					
						target->shot_maingun=0;	
						target->object_status=1;
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
				if (hit_location=="TR")
				{
					if (die_roll>=1 && die_roll<=3)
					{
						target->shot_maingun=0;	
						target->object_status=1;
					}
					else if (die_roll>=6 && die_roll<=7)
					{
						//app->scenario_interface->message6_hit_results=CL_String("Rear Machine Gun is damaged!");
					}
					else if (die_roll>=14 && die_roll<=15)
					{
						app->scenario_interface->message6_hit_results=CL_String("AA Machine Gun is damaged!");
					}
					else if (die_roll>=18 && die_roll<=20)
					{						
						target->suppressed=target->suppressed+1;
					}
					else
					{
						app->scenario_interface->message6_hit_results=CL_String("No effect!");
					}
				}
			}
		}
		else												//the target is infantry
		{
			if (source->shot_coax>0 && target->object_id==0 ||
				target->object_id==2)
			{
				app->scenario_interface->message6_hit_results=CL_String("No effect!");
			}

			if ((source->object_id==0 || source->object_id==1 || source->object_id==2) &&
				(target->object_id==3 || target->object_id==1 || target->object_id==4) && 
				source->shot_maingun>0)	
			{
				if (source->object_id==0)
					_penetration_value=6;		//HE round for T-34
				else if (source->object_id==1)
					 _penetration_value=3;		//HE round for T-70
				else if (source->object_id==2) 
					_penetration_value=13;		//120mm mortar round
			
				//2D6 die roll
				srand((unsigned)time(NULL));
				die_roll_six=rand()%12;

				fire_power=_penetration_value+die_roll_six;

				//std::cout << fire_power << std::endl;

				if (fire_power>=1 && fire_power<=7)
				{
					app->scenario_interface->message6_hit_results=CL_String("No effect!");
				}
				else if (fire_power>=8 && fire_power<=9)
				{
					srand((unsigned)time(NULL));
					die_roll=rand()%12;
					if (die_roll>target->morale)
					{
						target->do_astar=false;
						target->object_status=2;		//pinned
						target->movement_point=0;
						target->move0->stop();
					}
				}
				else if (fire_power>=10 && fire_power<=11)
				{
					target->do_astar=false;
					target->object_status=2;		//pinned
					target->movement_point=0;
					target->move0->stop();
				}
				else if (fire_power>=12 && fire_power<=13)
				{
					srand((unsigned)time(NULL));
					die_roll=rand()%12;
					if (die_roll>target->morale)
					{

						target->object_status=3;		//withdrawing
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
						if (source->x > target->x)
							target->x-=48;
						else if (source->x < target->x)
							target->x+=48;
						if (source->y > target->y)
							target->y-=48;
						else if (source->y < target->y)
							target->y+=48;
						if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
							target->x=target->x-48;
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
						//target->hull=target->hull_array[4];
						target->move0->stop();
					}
				}
				else if (fire_power>=14 && fire_power<=16)
				{
					app->scenario_interface->message6_hit_results=CL_String("One dead!");
					target->suppressed++;

					srand((unsigned)time(NULL));
					app->gdeadsoldier_number++;
					target->number_of_soldiers=target->number_of_soldiers-1;
					app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
					app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
					app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);

					switch (target->number_of_soldiers)
					{
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
				else if (fire_power>=17 && fire_power<=18)
				{
					srand((unsigned)time(NULL));
					die_roll=rand()%12;
					if (die_roll>target->morale)
					{					
						target->object_status=8;		//routed
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
						if (source->x > target->x)
							target->x-=96;
						else if (source->x < target->x)
							target->x+=96;
						if (source->y > target->y)
							target->y-=96;
						else if (source->y < target->y)
							target->y+=96;
						if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
							target->x=target->x-48;
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
						//target->hull=target->hull_array[4];
						target->shot_coax=0;
						target->shot_maingun=0;	
						target->move0->stop();
						target->suppressed++;
					}
				}
				else if (fire_power==19)
				{
					srand((unsigned)time(NULL));
					die_roll=rand()%12+2;
					if (die_roll>target->morale)
					{	
						app->scenario_interface->message6_hit_results=CL_String("One dead!");
						target->suppressed++;

						srand((unsigned)time(NULL));
						app->gdeadsoldier_number++;
						target->number_of_soldiers=target->number_of_soldiers-1;
						app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
						app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
						app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);

						switch (target->number_of_soldiers)
						{
						case 8:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
							break;
						case 7:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
							break;
						case 6:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
							break;
						case 5:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
							break;
						case 4:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
							break;
						case 3:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
							break;
						case 2:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
							break;
						case 1:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
							break;
						}

						if (target->number_of_soldiers<=0)
						{
							target->is_dead=true;
							target->is_visible=false;
						}
					}
				}
				else if (fire_power==20)
				{
					app->scenario_interface->message6_hit_results=CL_String("Two dead!");
					target->suppressed+=2;

					srand((unsigned)time(NULL));
					for (int j=0;j<2;j++)
					{
						app->gdeadsoldier_number++;
						app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
						app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
						app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);
					}
					
					target->number_of_soldiers=target->number_of_soldiers-2;
					
					switch (target->number_of_soldiers)
					{
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
				else if (fire_power>20)
				{
					app->scenario_interface->message6_hit_results=CL_String("Three are killed!");
					target->suppressed+=3;

					srand((unsigned)time(NULL));
					for (int j=0;j<3;j++)
					{
						app->gdeadsoldier_number++;
						app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
						app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
						app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);
					}

					target->number_of_soldiers=target->number_of_soldiers-3;
					
					switch (target->number_of_soldiers)
					{
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
			}
			else if ((source->object_id==0 || source->object_id==1 || source->object_id==2) && 
				(target->object_id==3 || target->object_id==1 || target->object_id==4) && 
				source->shot_coax>0)
			{
				if (source->object_id==0 || source->object_id==1)
					_penetration_value=5;		//T-34,T-70 CMG
				else if (source->object_id==3)
					_penetration_value=9-(2*(10-source->number_of_soldiers));		//russian infantry squad's fire power

				//2D6 die roll
				srand((unsigned)time(NULL));
				die_roll_six=rand()%12;

				fire_power=_penetration_value+die_roll_six;

				if (fire_power>=1 && fire_power<=7)
				{
					app->scenario_interface->message6_hit_results=CL_String("No effect!");
				}
				else if (fire_power>=8 && fire_power<=9)
				{
					srand((unsigned)time(NULL));
					die_roll=rand()%12;
					if (die_roll>target->morale)
					{
						target->object_status=2;		//pinned
						target->movement_point=0;
						target->move0->stop();
					}
				}
				else if (fire_power>=10 && fire_power<=11)
				{
					target->object_status=2;			//pinned
					target->movement_point=0;
					target->move0->stop();
				}
				else if (fire_power>=12 && fire_power<=13)
				{
					srand((unsigned)time(NULL));
					die_roll=rand()%12;
					if (die_roll>target->morale)
					{
						target->object_status=3;		//withdrawing
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
						if (source->x > target->x)
							target->x-=48;
						else if (source->x < target->x)
							target->x+=48;
						if (source->y > target->y)
							target->y-=48;
						else if (source->y < target->y)
							target->y+=48;
						if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
							target->x=target->x-48;
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
						//target->hull=target->hull_array[4];
						target->move0->stop();
					}
				}
				else if (fire_power>=14 && fire_power<=16)
				{
					app->scenario_interface->message6_hit_results=CL_String("One dead!");
					target->suppressed++;

					srand((unsigned)time(NULL));
					app->gdeadsoldier_number++;
					target->number_of_soldiers=target->number_of_soldiers-1;
					app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
					app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
					app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);

					switch (target->number_of_soldiers)
					{
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
				else if (fire_power>=17 && fire_power<=18)
				{
					srand((unsigned)time(NULL));
					die_roll=rand()%12;
					if (die_roll>target->morale)
					{
						target->object_status=8;		//routed
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
						if (source->x > target->x)
							target->x-=96;
						else if (source->x < target->x)
							target->x+=96;
						if (source->y > target->y)
							target->y-=96;
						else if (source->y < target->y)
							target->y+=96;
						if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
							target->x=target->x-48;
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
						//target->hull=target->hull_array[4];
						target->shot_coax=0;
						target->shot_maingun=0;
						target->suppressed++;
						target->move0->stop();
					}
				}
				else if (fire_power==19)
				{
					srand((unsigned)time(NULL));
					die_roll=rand()%12+2;
					if (die_roll>target->morale)
					{
						app->scenario_interface->message6_hit_results=CL_String("One dead!");
						target->suppressed++;

						srand((unsigned)time(NULL));
						app->gdeadsoldier_number++;
						target->number_of_soldiers=target->number_of_soldiers-1;
						app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
						app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
						app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);
					}

					switch (target->number_of_soldiers)
					{
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
				else if (fire_power==20)
				{
					app->scenario_interface->message6_hit_results=CL_String("Two dead!");
					target->suppressed+=2;

					srand((unsigned)time(NULL));
					for (int j=0;j<2;j++)
					{
						app->gdeadsoldier_number++;
						app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
						app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
						app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);
					}
				
					target->number_of_soldiers=target->number_of_soldiers-2;
					
					switch (target->number_of_soldiers)
					{
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
				else if (fire_power>20)
				{
					app->scenario_interface->message6_hit_results=CL_String("Three are killed!");
					target->suppressed+=3;

					srand((unsigned)time(NULL));
					for (int j=0;j<3;j++)
					{
						app->gdeadsoldier_number++;
						app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
						app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
						app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);
					}

					target->number_of_soldiers=target->number_of_soldiers-3;
					
					switch (target->number_of_soldiers)
					{
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
			}
		}
	}
	//It missed...
	else
	{
		if (source->object_id==3)
		{
			//2D6 die roll
			srand((unsigned)time(NULL));
			die_roll_six=rand()%12;

			if (source->object_id==3)
					_penetration_value=9-(2*(10-source->number_of_soldiers));		//russian infantry squad's fire power

			if (dist>=6 || source->object_status==2 || source->object_status==3)
				_penetration_value=_penetration_value/2;

			if (target->object_id==0 || target->object_id==2)		//Pz.IVJ, Marder IIIG tanks
				fire_power=_penetration_value+die_roll_six-4;
			else 
				fire_power=_penetration_value+die_roll_six;

			if (fire_power>=1 && fire_power<=7)
			{
				app->scenario_interface->message6_hit_results=CL_String("No effect!");
			}
			else if (fire_power>=8 && fire_power<=9)
			{
				srand((unsigned)time(NULL));
				die_roll=rand()%12;
				if (die_roll>target->morale)
				{
					if (target->object_id!=0)
					{
						target->do_astar=false;
						target->object_status=2;		//pinned
						target->movement_point=0;
						target->move0->stop();
						app->scenario_interface->message6_hit_results=CL_String("Enemy is Pinned!");
					}
				}
			}
			else if (fire_power>=10 && fire_power<=11)
			{
				if (target->object_id!=0)
				{
					app->scenario_interface->message6_hit_results=CL_String("Enemy is Pinned!");
					target->do_astar=false;
					target->object_status=2;			//pinned
					target->movement_point=0;
					target->move0->stop();
				}
			}
			else if (fire_power>=12 && fire_power<=13)
			{
				srand((unsigned)time(NULL));
				die_roll=rand()%12;
				if (die_roll>target->morale)
				{
					if (target->object_id!=0)
					{
						target->do_astar=false;
						target->object_status=3;		//withdrawing
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
						if (source->x > target->x)
							target->x-=48;
						else if (source->x < target->x)
							target->x+=48;
						if (source->y > target->y)
							target->y-=48;
						else if (source->y < target->y)
							target->y+=48;
						if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
							target->x=target->x-48;
						app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
						//target->hull=target->hull_array[4];
						target->move0->stop();
					}
					else
						target->suppressed++;
				}
			}
			else if (fire_power>=14 && fire_power<=16)
			{
				if (target->object_id==1 || target->object_id==3 ||
					target->object_id==4)
				{
					srand((unsigned)time(NULL));
					app->scenario_interface->message6_hit_results=CL_String("One dead!");
					target->suppressed++;
					app->gdeadsoldier_number++;
					target->number_of_soldiers--;
					app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
					app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
					app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);

					switch (target->number_of_soldiers)
					{
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
				else
					target->suppressed+=2;
			}
			else if (fire_power>=17 && fire_power<=18)
			{
				srand((unsigned)time(NULL));
				die_roll=rand()%12;
				if (die_roll>target->morale)
				{
					target->do_astar=false;
					target->object_status=8;		//routed
					app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='|';
					if (source->x > target->x)
							target->x-=96;
						else if (source->x < target->x)
							target->x+=96;
						if (source->y > target->y)
							target->y-=96;
						else if (source->y < target->y)
							target->y+=96;
					if (app->scenario_map->get_drive_dir((int)target->x/TILE_WIDTH,(int)target->y/TILE_HEIGHT)=='#')
						target->x=target->x-48;
					app->scenario_map->drive_map[(int)(target->y+24)/TILE_HEIGHT][(int)(target->x+24)/TILE_WIDTH]='#';
					//target->hull=target->hull_array[4];
					target->shot_coax=0;
					target->shot_maingun=0;
					target->move0->stop();
				}
			}
			else if (fire_power==19)
			{
				srand((unsigned)time(NULL));
				die_roll=rand()%12+2;
				if (die_roll>target->morale)
				{
					if (target->object_id==1 || target->object_id==3 ||
						target->object_id==4)
					{
						srand((unsigned)time(NULL));
						app->scenario_interface->message6_hit_results=CL_String("One dead!");
						target->suppressed++;
						app->gdeadsoldier_number++;
						target->number_of_soldiers=target->number_of_soldiers-1;
						app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
						app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
						app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);

						switch (target->number_of_soldiers)
						{
						case 8:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
							break;
						case 7:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
							break;
						case 6:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
							break;
						case 5:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
							break;
						case 4:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
							break;
						case 3:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
							break;
						case 2:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
							break;
						case 1:
							target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
							target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
							break;
						}

						if (target->number_of_soldiers<=0)
						{
							target->is_dead=true;
							target->is_visible=false;
						}
					}
					else
						target->suppressed+=2;
				}
			}
			else if (fire_power==20)
			{
				target->suppressed+=2;

				if (target->object_id==1 || target->object_id==3 ||
					target->object_id==4)
				{
					srand((unsigned)time(NULL));
					app->scenario_interface->message6_hit_results=CL_String("Two dead!");
					for (int j=0;j<2;j++)
					{
						app->gdeadsoldier_number++;
						app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
						app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
						app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);
					}
					
					target->number_of_soldiers=target->number_of_soldiers-2;
					
					switch (target->number_of_soldiers)
					{
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
			}
			else if (fire_power>20)
			{
				target->suppressed+=3;

				if (target->object_id==1 || target->object_id==3 ||
					target->object_id==4)
				{
					srand((unsigned)time(NULL));
					app->scenario_interface->message6_hit_results=CL_String("Three are killed!");
					for (int j=0;j<3;j++)
					{
						app->gdeadsoldier_number++;
						app->deadbody_frame[app->gdeadsoldier_number]=rand()%4;
						app->gdeadsoldier_coordinates_x[app->gdeadsoldier_number]=target->x+(rand()%60);
						app->gdeadsoldier_coordinates_y[app->gdeadsoldier_number]=target->y+(rand()%60);
					}

					target->number_of_soldiers=target->number_of_soldiers-3;
					
					switch (target->number_of_soldiers)
					{
					case 8:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
						break;
					case 7:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
						break;
					case 6:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
						break;
					case 5:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
						break;
					case 4:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
						break;
					case 3:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
						break;
					case 2:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
						break;
					case 1:
						target->shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
						target->hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
						break;
					}

					if (target->number_of_soldiers<=0)
					{
						target->is_dead=true;
						target->is_visible=false;
					}
				}
			}
		}
		else
			app->scenario_interface->message4_hit_results=CL_String("It missed...");	
	}
}

