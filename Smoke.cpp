/***************************************************************************
                          Smoke.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "Main.h"
#include "Smoke.h"

Smoke::Smoke(Kursk* app)
{
	smoke_surface=CL_Surface::load("Graphics/Objects/smoke",app->resources);

	smoke_x=0;
	smoke_y=0;
	smoke_frame=0;
	smoke_frame_delay=0;
}

void Smoke::draw(Kursk* app)
{
	smoke_surface->put_screen(smoke_x-app->map_x,smoke_y-app->map_y,smoke_frame);

	if (CL_System::get_time()>=smoke_frame_delay+300)
	{
		smoke_frame++;
		smoke_frame_delay=CL_System::get_time();
	}

	if (smoke_frame>5)
		smoke_frame=0;
}
