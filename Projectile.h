/***************************************************************************
                          Projectile.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _PROJECTILE_H_
#define _PROJECTILE_H_
#define FC __fastcall

class GameObject;

class projectile
{
private:
	virtual void calculate_trajectory();

	CL_Surface* projectile_surface;
	int* trajectory_x;
	int* trajectory_y;
	int counter;

protected:

public:
	projectile(Kursk* app,GameObject* source,GameObject* target);
	virtual ~projectile();

	virtual void show(Kursk* app);
	virtual void update(Kursk* app);

	CL_Surface* explosion;
	int start_x,start_y;
	int dest_x,dest_y;	
	int shell_x,shell_y;
	bool is_dead;
	int frame_counter;
	int projectile_frame_delay;
};

#undef FC
#endif