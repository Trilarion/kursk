/***************************************************************************
                          Weapons_selector.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <fstream.h>
#include "Main.h"
#include "weapons_selector.h"
#include "Objects.h"

void status(int _select, Kursk* app);

weapons_selector::weapons_selector(Kursk*_app)
{
	app=_app;
	list_top=-4;
	list_bottom=7;
}

CL_String w_read_line(CL_InputSource*in)
{
	CL_String ret;
	static char c[2]={ 0,0 };

	do
	{
		c[0]=in->read_char8();
		switch(c[0])
		{
		case 13:
			c[0]=in->read_char8();
			break;
		case 10:
			break;
		default:
			ret+=c;
			break;
		}
	} while(c[0]!=10);

	return ret;
}

void weapons_selector::load_weapons_list(CL_String list_path)
{

	CL_InputSourceProvider*input=CL_InputSourceProvider::create_file_provider("");
	CL_InputSource*file=input->open_source(list_path);

	bool a=true;
	while(a)
	{
		CL_String fname=w_read_line(file);
		if(fname=="#end")
		{
			a=false;
		}
		else
			weapons.push_back(new weapons_list_entry(fname));
	}
}

void weapons_selector::show_weapons_list()
{
	int j=0;
	int i=0;

	app->simple_font_yellow->print_left(435,248,CL_String(app->requisition_point));
	app->weapons_list->put_screen(63,63);
	for(std::list<weapons_list_entry*>::iterator t_list=weapons.begin();
	t_list!=weapons.end();t_list++)
	{
		if(i>=list_top&&i<=list_bottom)
		{
			if((*t_list)->selected())
			{
				CL_Display::fill_rect(66,64+(j*12),268,(64+(j*12))+12,0.0f,0.3f,0.0f,0.6f);
			}

			CL_String name=(*t_list)->get_name();
			if (j>=0 && j<4)
			{
				app->simple_font_yellow->print_left(66,66+(j*12),name.mid(0,25));
				app->simple_font_yellow->print_right(270,66+(j*12),name.right(2));
			}
			else
			{
				app->simple_font->print_left(66,66+(j*12),name.mid(0,25));
				app->simple_font->print_right(270,66+(j*12),name.right(2));
			}
			j++;
		}
		i++;
	}
}

void weapons_selector::check_input()
{
	CL_String req_point;
	int price=0;
	int l;
	ifstream fin;

	// show the weapons picture if the row is in focus
	if(CL_Mouse::get_x()>66&&CL_Mouse::get_x()<268
		&&CL_Mouse::get_y()>66&&CL_Mouse::get_y()<152)
	{
		int select=(((int)CL_Mouse::get_y()-12)/12);
		select+=list_top;

		switch (select)
		{
		case 0:
		case 1:
		case 2:
		case 3:
			app->current_id=0;
			app->weapons_picture=CL_Surface::load("Graphics/Weapons/pzIII",app->resources);
			app->weapons_picture->put_screen(285,280);
			app->gpzivj_damage=CL_Surface::load("Graphics/Interface/gpzivj_damage",app->resources);
			app->gpzivj_damage->put_screen(288,68);
			app->damage_font_white->print_left(522,293,"75mm KwK48");
			app->damage_font_white->print_left(522,305,"2x MG34");
			app->damage_font_white->print_left(534,329,"87");
			app->damage_font_white->print_left(534,341,"3150");
			//front armor
			app->damage_font_white->print_left(520,396,"80");
			app->damage_font_white->print_left(520,408,"50");
			//side armor
			app->damage_font_white->print_left(552,396,"30");
			app->damage_font_white->print_left(552,408,"30");
			//rear armor
			app->damage_font_white->print_left(585,396,"20");
			app->damage_font_white->print_left(585,408,"30");
			status(select,app);
			//short description
			fin.open("Dat/pzivh_descr.txt");
			for (l=0;l<=8;l++)
			{
				fin >> unit_description[l];
				app->description_font_yellow->print_left(66,(320+10*l),unit_description[l]);
			}
			fin.close();
			break;
		case 4:
			app->current_id=1;
			app->weapons_picture=CL_Surface::load("Graphics/Weapons/hvyweapsqd",app->resources);
			app->weapons_picture->put_screen(282,279);
			app->gpzivj_damage=CL_Surface::load("Graphics/Interface/hws_damage",app->resources);
			app->gpzivj_damage->put_screen(288,68);
			//short description
			fin.open("Dat/hws_descr.txt");
			for (l=0;l<=8;l++)
			{
				fin >> unit_description[l];
				app->description_font_yellow->print_left(66,(320+10*l),unit_description[l]);
			}
			fin.close();
			break;
		case 5:
			app->current_id=2;
			app->weapons_picture=CL_Surface::load("Graphics/Weapons/pzIV",app->resources);
			app->weapons_picture->put_screen(285,280);
			app->gpzivj_damage=CL_Surface::load("Graphics/Interface/marderiii_damage",app->resources);
			app->gpzivj_damage->put_screen(288,68);
			app->damage_font_white->print_left(522,293,"75mm PaK40");
			app->damage_font_white->print_left(522,305,"1x MG34");
			app->damage_font_white->print_left(534,329,"27");
			app->damage_font_white->print_left(534,341,"600");
			//front armor
			app->damage_font_white->print_left(520,396,"15");
			app->damage_font_white->print_left(520,408,"10");
			//side armor
			app->damage_font_white->print_left(552,396,"15");
			app->damage_font_white->print_left(552,408,"10");
			//rear armor
			app->damage_font_white->print_left(585,396,"10");
			app->damage_font_white->print_left(585,408,"10");
			status(select,app);
			//short description
			fin.open("Dat/marder_descr.txt");
			for (l=0;l<=7;l++)
			{
				fin >> unit_description[l];
				app->description_font_yellow->print_left(66,(320+10*l),unit_description[l]);
			}
			fin.close();
			break;
		case 6:
			app->current_id=3;
			app->weapons_picture=CL_Surface::load("Graphics/Weapons/gmortar80",app->resources);
			app->weapons_picture->put_screen(282,279);
			app->gpzivj_damage=CL_Surface::load("Graphics/Interface/mortar_damage",app->resources);
			app->gpzivj_damage->put_screen(288,68);
			status(select,app);
			//short description
			fin.open("Dat/gmortar_descr.txt");
			for (l=0;l<=5;l++)
			{
				fin >> unit_description[l];
				app->description_font_yellow->print_left(66,(320+10*l),unit_description[l]);
			}
			fin.close();
			break;
		
		}
	}

	// left_click on weapons name
	if(CL_Mouse::left_pressed())
	{
		if(CL_Mouse::get_x()>66&&CL_Mouse::get_x()<268
			&&CL_Mouse::get_y()>66&&CL_Mouse::get_y()<152)
		{
			int selection=(((int)CL_Mouse::get_y()-12)/12);
			selection+=list_top;

			std::list<weapons_list_entry*>::iterator it=weapons.begin();
			for(int i=0; i<selection; i++)
				it++;

			if(selection<weapons.size()&&selection>-1)
			{
				if((*it)->selected()==false)
				{   
					//let's calculate the remaining req.points
					CL_String name=(*it)->get_name();
					req_point=name.right(2);
					price=req_point.get_as_int();
					app->requisition_point=app->requisition_point-price;
					app->number_of_weapons++;
					app->selected_weapons_id[selection]=app->current_id;

					if (app->requisition_point<=0)
					{
						app->number_of_weapons--;
						app->selected_weapons_id[selection]=255;
						app->requisition_point=app->requisition_point+price;
					}
					else
					{
						app->selected_weapons.push_back((*it));
						(*it)->set_selected(true);
					}
				}
			}
		}
	}
	// right_pressed on weapons name
	if(CL_Mouse::middle_pressed())
	{
		if(CL_Mouse::get_x()>66&&CL_Mouse::get_x()<268
			&&CL_Mouse::get_y()>66&&CL_Mouse::get_y()<152)
		{
			int selection=(((int)CL_Mouse::get_y()-12)/12);
			selection+=list_top;

			std::list<weapons_list_entry*>::iterator it=weapons.begin();
			for(int i=0; i<selection; i++)
				it++;

			if(selection<weapons.size())
			{
				if((*it)->selected()==true)
				{
					// unselect selected
					(*it)->set_selected(false);

					//let's calculate the remaining req.points
					CL_String name=(*it)->get_name();
					req_point=name.right(2);
					price=req_point.get_as_int();
					app->requisition_point=app->requisition_point+price;
					app->number_of_weapons--;
					app->selected_weapons_id[selection]=255;

					// clear selected_tracks
					app->selected_weapons.clear();
					// and add all selected to selected_tracks
					for(std::list<weapons_list_entry*>::iterator t_list=weapons.begin();
					t_list!=weapons.end();t_list++)
					{
						if((*t_list)->selected()==true)
							app->selected_weapons.push_back((*t_list));
					}
				}
			}
		}
	}
}

void weapons_selector::draw()
{
	int j=0;

	app->scenario_background->put_screen(0,0);
	app->damage_control->put_screen(284,61);
	app->simple_font->print_center(320,465,"After selecting your support units, press the Requisition button.");
	show_weapons_list();
}

weapons_list_entry::weapons_list_entry(CL_String _name)
{
	weapons_name=_name;
	t_selected=false;
}

void status(int _select, Kursk* app)
{
	//initial status 
	switch (_select)
	{
	case 0:
		app->mini_font->print_left(400,74,"Mobile");
		app->damage_font_white->print_left(343,97,"40");
		app->damage_font_white->print_left(359,97,"47");
		app->damage_font_white->print_left(430,106,"2250");
		app->damage_font_green->print_left(286,114,"Functional");
		app->damage_font_green->print_left(374,114,"Functional");
		app->damage_font_white->print_left(286,127,"Strumberger");
		app->damage_font_white->print_left(375,127,"Cmdr.");
		app->mini_font->print_left(430,127,"OK");
		app->damage_font_white->print_left(286,143,"Tesch");
		app->damage_font_white->print_left(375,143,"Gunner");
		app->mini_font->print_left(430,143,"OK");
		app->damage_font_white->print_left(286,158,"Studer");
		app->damage_font_white->print_left(375,158,"Loader");
		app->mini_font->print_left(430,158,"OK");
		app->damage_font_white->print_left(286,173,"Tillmann");
		app->damage_font_white->print_left(375,173,"Driver");
		app->mini_font->print_left(430,173,"OK");
		app->damage_font_white->print_left(286,188,"Hecht");
		app->damage_font_white->print_left(375,188,"Asst.Drvr.");
		app->mini_font->print_left(430,188,"OK");
		break;
	case 1:
		app->mini_font->print_left(400,74,"Mobile");
		app->damage_font_white->print_left(343,97,"40");
		app->damage_font_white->print_left(359,97,"47");
		app->damage_font_white->print_left(430,106,"2250");
		app->damage_font_green->print_left(286,114,"Functional");
		app->damage_font_green->print_left(374,114,"Functional");
		app->damage_font_white->print_left(286,127,"Baumgartner");
		app->damage_font_white->print_left(375,127,"Cmdr.");
		app->mini_font->print_left(430,127,"OK");
		app->damage_font_white->print_left(286,143,"Griesmayer");
		app->damage_font_white->print_left(375,143,"Gunner");
		app->mini_font->print_left(430,143,"OK");
		app->damage_font_white->print_left(286,158,"Geiser");
		app->damage_font_white->print_left(375,158,"Loader");
		app->mini_font->print_left(430,158,"OK");
		app->damage_font_white->print_left(286,173,"Kreisch");
		app->damage_font_white->print_left(375,173,"Driver");
		app->mini_font->print_left(430,173,"OK");
		app->damage_font_white->print_left(286,188,"Holzer");
		app->damage_font_white->print_left(375,188,"Asst.Drvr.");
		app->mini_font->print_left(430,188,"OK");
		break;
	case 2:
		app->mini_font->print_left(400,74,"Mobile");
		app->damage_font_white->print_left(343,97,"40");
		app->damage_font_white->print_left(359,97,"47");
		app->damage_font_white->print_left(430,106,"2250");
		app->damage_font_green->print_left(286,114,"Functional");
		app->damage_font_green->print_left(374,114,"Functional");
		app->damage_font_white->print_left(286,127,"Jantzen");
		app->damage_font_white->print_left(375,127,"Cmdr.");
		app->mini_font->print_left(430,127,"OK");
		app->damage_font_white->print_left(286,143,"Holzhauer");
		app->damage_font_white->print_left(375,143,"Gunner");
		app->mini_font->print_left(430,143,"OK");
		app->damage_font_white->print_left(286,158,"Janz");
		app->damage_font_white->print_left(375,158,"Loader");
		app->mini_font->print_left(430,158,"OK");
		app->damage_font_white->print_left(286,173,"Remlinger");
		app->damage_font_white->print_left(375,173,"Driver");
		app->mini_font->print_left(430,173,"OK");
		app->damage_font_white->print_left(286,188,"Ahrens");
		app->damage_font_white->print_left(375,188,"Asst.Drvr.");
		app->mini_font->print_left(430,188,"OK");
		break;
	case 3:
		app->mini_font->print_left(400,74,"Mobile");
		app->damage_font_white->print_left(343,97,"40");
		app->damage_font_white->print_left(359,97,"47");
		app->damage_font_white->print_left(430,106,"2250");
		app->damage_font_green->print_left(286,114,"Functional");
		app->damage_font_green->print_left(374,114,"Functional");
		app->damage_font_white->print_left(286,127,"Bauer");
		app->damage_font_white->print_left(375,127,"Cmdr.");
		app->mini_font->print_left(430,127,"OK");
		app->damage_font_white->print_left(286,143,"Bieber");
		app->damage_font_white->print_left(375,143,"Gunner");
		app->mini_font->print_left(430,143,"OK");
		app->damage_font_white->print_left(286,158,"Woll");
		app->damage_font_white->print_left(375,158,"Loader");
		app->mini_font->print_left(430,158,"OK");
		app->damage_font_white->print_left(286,173,"Michler");
		app->damage_font_white->print_left(375,173,"Driver");
		app->mini_font->print_left(430,173,"OK");
		app->damage_font_white->print_left(286,188,"Merscher");
		app->damage_font_white->print_left(375,188,"Asst.Drvr.");
		app->mini_font->print_left(430,188,"OK");
		break;
	case 4:
		app->mini_font->print_left(400,74,"Mobile");
		app->damage_font_white->print_left(343,97,"25");
		app->damage_font_white->print_left(359,97,"15");
		app->damage_font_white->print_left(430,106,"600");
		app->damage_font_green->print_left(286,114,"Functional");
		app->damage_font_green->print_left(374,114,"Functional");
		
		break;
	case 5:
		app->mini_font->print_left(400,74,"Mobile");
		app->damage_font_white->print_left(343,97,"10");
		app->damage_font_white->print_left(359,97,"17");
		app->damage_font_white->print_left(430,106,"2000");
		app->damage_font_green->print_left(286,114,"Functional");
		app->damage_font_green->print_left(374,114,"Functional");
		
		break;
	case 6:
		//app->mini_font->print_left(400,74,"Mobile");
		app->damage_font_white->print_left(343,97,"-");
		app->damage_font_white->print_left(359,97,"30");
		//app->damage_font_white->print_left(430,106,"600");
		app->damage_font_green->print_left(286,114,"Functional");
		//app->damage_font_green->print_left(374,114,"Functional");
		
		break;
	}
	//this after the first scenario
	//*****************************
	//for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
	//{
	//	if ((*it)->object_index==_select)
	//	{
	//		switch ((*it)->object_status)
	//		{
	//		case 0:
	//			app->simple_font->print_left(300,66,"Mobile");
	//			break;
	//		}
	//	}
	//}
}