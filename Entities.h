#ifndef _ENTITIES_H_
#define _ENTITIES_H_

class Kursk;

class GameObject
{
protected:
	Kursk* app;
	bool is_turret;

public:
	GameObject(int _object_id);
	virtual ~GameObject() { return; }
   
	virtual void show(int x,int y,int hull_frame,int turret_frame);	// draw object on screen
	virtual void update();			// Moves the object around

	CL_Surface* hull;
	CL_Surface* turret;

};

#endif