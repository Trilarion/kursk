/***************************************************************************
                          Main.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <time.h>
#include <fstream.h>
#include <string.h>
#include "Main.h"
#include "Menu.h"
#include "scenario_selector.h"
#include "map.h"
#include "interface.h"
#include "minimap.h"
#include "Objects.h"
#include "Path.h"
#include "Miscellaneous.h"
#include "Smoke.h"
#include "version_control.h"

Kursk app;

void Kursk::run_scenario()
{
	int y;

	dead_units=0;

	//move the map according to the mouse cursor
	if ((CL_Mouse::get_x()>=639)&&(map_x<1440))
		map_x=map_x+8;
	else if ((CL_Mouse::get_x()<=0)&&(map_x>0))
		map_x=map_x-8;
	else if ((CL_Mouse::get_y()>=479)&&(map_y<1003))
		map_y=map_y+8;
	else if ((CL_Mouse::get_y()<=0)&&(map_y>0))
		map_y=map_y-8;

	if (game_state!=drag_and_drop)
	{
		// draw the background
		for (y=0+(map_y/TILE_HEIGHT);y<VIEWPORT_HEIGHT+(map_y/TILE_HEIGHT);y++)
		{
			for (int x=0+(map_x/TILE_WIDTH);x<VIEWPORT_WIDTH+(map_x/TILE_WIDTH);x++)
			{
				int scr_x=x*TILE_WIDTH-map_x;
				int scr_y=y*TILE_HEIGHT-map_y;
				map_kursk001->put_screen(scr_x,scr_y,(x+y)+(y*(MAP_WIDTH)));
			}
		}
		//put the mortar craters on the terrain
		if (mortarshot_number>0)
		{
			for (int i=1; i<=mortarshot_number-1;i++)
			{
				mortar80_crater->put_screen(mortarshell_coordinates_x[i]-map_x,
					mortarshell_coordinates_y[i]-map_y);
			}
		}
		if (crater_visible)
		{
			mortar80_crater->put_screen(mortarshell_coordinates_x[mortarshot_number]-map_x,
					mortarshell_coordinates_y[mortarshot_number]-map_y);
		}
		//put the dead bodies on the terrain
		if (gdeadsoldier_number>0)
		{
			for (int i=1;i<=gdeadsoldier_number;i++)
			{
				gdeadsoldier->put_screen(gdeadsoldier_coordinates_x[i]-map_x,
					gdeadsoldier_coordinates_y[i]-map_y,deadbody_frame[i]);
			}
		}
		if (rdeadsoldier_number>0)
		{
			for (int i=1;i<=rdeadsoldier_number;i++)
			{
				rdeadsoldier->put_screen(rdeadsoldier_coordinates_x[i]-map_x,
					rdeadsoldier_coordinates_y[i]-map_y,rdeadbody_frame[i]);
			}
		}
		//blit the victory locations
		//german
		if (scenario_map->victory_flag1==0)
			r_victory_point->put_screen(1728-map_x,192-map_y);
		else
			g_victory_point->put_screen(1728-map_x,192-map_y);
		if (scenario_map->victory_flag2==0)
			r_victory_point->put_screen(1488-map_x,624-map_y);
		else
			g_victory_point->put_screen(1488-map_x,624-map_y);
		if (scenario_map->victory_flag3==0)
			r_victory_point->put_screen(1824-map_x,1104-map_y);
		else
			g_victory_point->put_screen(1824-map_x,1104-map_y);
		//russian
		g_victory_point->put_screen(48-map_x,384-map_y);
		g_victory_point->put_screen(144-map_x,768-map_y);

		//************************************************************
		// here goes the object's update
		//************************************************************
		//blit the sprites with top priority (tanks,infantry...)
		for (std::list<GameObject*>::iterator it=objects.begin();it!=objects.end();it++)
		{
			//put the sandbags
			if ((*it)->draw_sandbag)
			{
				switch ((*it)->object_index)
				{
				case 0:
					sandbag->put_screen(1536-7-map_x,1104-7-map_y);
					break;
				case 1:
					sandbag->put_screen(1824-7-map_x,480-7-map_y);
					break;
				case 2:
					sandbag->put_screen(1824-7-map_x,576-7-map_y);
					break;
				case 4:
					sandbag->put_screen(1488-7-map_x,384-7-map_y);
					break;
				}
			}

			if ((*it)->blt_priority==0)
			{
				(*it)->update(this);
				(*it)->show((*it)->x-map_x,(*it)->y-map_y,this);
			}
		}
		//blit secondary sprites (blasts,fire,smoke...)	
		for (std::list<GameObject*>::iterator item=objects.begin();item!=objects.end();item++)
		{
			if ((*item)->blt_priority==1)
			{
				(*item)->update(this);
				(*item)->show((*item)->x-map_x,(*item)->y-map_y,this);
			}
			//check if all player units are dead, thus we end the scenario
			if ((*item)->is_german && ((*item)->is_dead || (*item)->object_status==9))
				dead_units++;
		}

		//draw the overlay (trees)
		for (y=0+(map_y/TILE_HEIGHT);y<VIEWPORT_HEIGHT+(map_y/TILE_HEIGHT);y++)
		{
			for (int x=0+(map_x/TILE_WIDTH);x<VIEWPORT_WIDTH+(map_x/TILE_WIDTH);x++)
			{
				int scr_x=x*TILE_WIDTH-map_x;
				int scr_y=y*TILE_HEIGHT-map_y;
				map_trees001->put_screen(scr_x,scr_y,(x+y)+(y*(MAP_WIDTH)));
			}
		}
	}
	else
	{
		for (y=0+(map_y/TILE_HEIGHT);y<VIEWPORT_HEIGHT+(map_y/TILE_HEIGHT);y++)
		{
			for (int x=0+(map_x/TILE_WIDTH);x<VIEWPORT_WIDTH+(map_x/TILE_WIDTH);x++)
			{
				int scr_x=x*TILE_WIDTH-map_x;
				int scr_y=y*TILE_HEIGHT-map_y;
				map_mask001->put_screen(scr_x,scr_y,(x+y)+(y*(MAP_WIDTH)));
			}
		}
		//************************************************************
		// here goes the object's update
		//************************************************************
		for (std::list<GameObject*>::iterator it=objects.begin();it!=objects.end();it++)
		{
			(*it)->update(this);
			(*it)->show((*it)->x-map_x,(*it)->y-map_y,this);
		}
	}

	//put smoke onto the terrain
	if (smoke_number>0)
	{
		for (int i=1;i<=smoke_number;i++)
		{
			smoke[i]->draw(this);
		}
	}

	scenario_interface->draw();

	//we quit, if we exceed the max. turn/scenario
	if (turn>scenario_turn || dead_units>=number_of_germans)
		game_state=end_scenario;
}

bool Kursk::init_objects()
{
	srand((unsigned)time(NULL));

	mortarshot_number=0;
	gdeadsoldier_number=0;
	rdeadsoldier_number=0;
	crater_visible=false;
	number_of_russians=6;
	number_of_germans=0;
	smoke_number=0;
	turn=1;					//First turn 
	russian_turn=false;

	//German names
	char names[285][20];
	
	ifstream fin("Dat/gnames.txt");
	for (int j=0;j<85;j++)
		fin >> names[j];
	fin.close();

	scenario_map=new tileMap();
	//std::cout << "created tileMap()" <<std::endl;

	CL_String path="scenarios/";
	for(std::list<scenario_list_entry*>::iterator t_list=selected_scenario.begin();
		t_list!=selected_scenario.end();t_list++)
	{
		if((*t_list)->selected()==true)
		{
			path+=(*t_list)->get_name();
			scenario_map->load_map(path);
			//std::cout << "map loaded successfully" << std::endl;
			break;
		}
		else
			return false;
			
	}
	CL_MouseCursor::hide();
	CL_Display::clear_display();
	CL_Surface* loading_bg=CL_TargaProvider::create("Gfx/loading_bg.tga",NULL);
	loading_bg->put_screen(160,224);
	simple_font->print_left(235,235,"Generating Russian Units...");
	CL_Display::flip_display();
	//initialize the russian units
	//for the first scenario
	int _russian_object_id[]={0,0,0,1,1,2,3,3,3};

	switch (scenario_number)
	{
	case 0:
		{for (int i=0;i<9;i++)
			objects.push_back(new GameObject_Russian(0,0,_russian_object_id[i],i,this));
		}
		break;

	case 1:
		break;
	}
	CL_Display::clear_display();
	loading_bg->put_screen(160,224);
	simple_font->print_left(235,235,"Generating German Units...");
	CL_Display::flip_display();
	//initialize game objects
	int index=0;
	selected_count=1;
	for (int i=0;i<100;i++)
	{
		if (selected_weapons_id[i]!=255)
		{
			objects.push_back(new GameObject_German(20,20+(65*i),selected_weapons_id[i],index,this));
			number_of_germans++;
			index++;
		}
	}
	//initialize each unit basic data structure
	{for (std::list<GameObject*>::iterator it=objects.begin();it!=objects.end();it++)
		if ((*it)->is_german)
			(*it)->commander=names[rand()%85];
	}
	CL_MouseCursor::show();
	delete loading_bg;

	return true;
}

void Kursk::objects_clean_up()
{
	delete scenario_map;
}

char* Kursk::get_title()
{
	static char str[80];

	sprintf(str,"%s v%d.%d - Release %d",APP_NAME,
		APP_VER_MAJOR,APP_VER_MINOR,APP_BUILD);
	
	return str;
}

void Kursk::init_modules()
{
	CL_SetupCore::init();
	CL_SetupCore::init_display();
}

void Kursk::deinit_modules()
{
	CL_SetupCore::deinit();
	CL_SetupCore::deinit_display();
	CL_SetupCore::deinit_sound();
}

void Kursk::init_resources()
{
	mainmenu_background	=CL_Surface::load("Graphics/Menu/mainmenu_background", resources);
	button_down			=CL_Surface::load("Graphics/Menu/button_down",resources);
	cursor_provider		=CL_MouseCursorProvider::create(CL_SurfaceProvider::load("Graphics/Menu/mouse_pointer",resources),true,false,NULL);	
	CL_MouseCursor::set_cursor(cursor_provider,0);
	mouse_pointer		=CL_Surface::load("Graphics/Menu/mouse_pointer",resources);
	scenario_background =CL_Surface::load("Graphics/Menu/scenario_background",resources);
	damage_control		=CL_Surface::load("Graphics/Interface/damage_control",resources);
	weapons_list		=CL_Surface::load("Graphics/Interface/weapons_list",resources);
	map_kursk001		=CL_Surface::load("Graphics/Map/map_kursk001",resources);
	map_trees001		=CL_Surface::load("Graphics/Map/map_trees001",resources);
	map_mask001			=CL_Surface::load("Graphics/Map/map_mask001",resources);
	r_victory_point		=CL_Surface::load("Graphics/Map/r_victory_point",resources);
	g_victory_point		=CL_Surface::load("Graphics/Map/g_victory_point",resources);
	mortar80_crater		=CL_Surface::load("Graphics/Map/mortar80_crater",resources);
	gdeadsoldier		=CL_Surface::load("Graphics/Objects/gdeadsoldier",resources);
	rdeadsoldier		=CL_Surface::load("Graphics/Objects/rdeadsoldier",resources);
	scenario_lost		=CL_Surface::load("Graphics/Objects/scenario_lost",resources);
	simple_font			=CL_Font::load("Fonts/simple_font",resources);
	simple_font_grey	=CL_Font::load("Fonts/simple_font_grey",resources);
	mini_font			=CL_Font::load("Fonts/mini_font",resources);
	simple_font_yellow	=CL_Font::load("Fonts/simple_font_yellow",resources);
	damage_font_white	=CL_Font::load("Fonts/damage_font_white",resources);
	damage_font_yellow	=CL_Font::load("Fonts/damage_font_yellow",resources);
	damage_font_green	=CL_Font::load("Fonts/damage_font_green",resources);
	damage_font_red		=CL_Font::load("Fonts/damage_font_red",resources);
	description_font_yellow=CL_Font::load("Fonts/description_font_yellow",resources);
	sandbag				=CL_Surface::load("Graphics/Objects/sandbag",resources);

	CL_MouseCursor::show();

	//std::cout << "init_resources() - done!" << std::endl;
}

void Kursk::deinit()
{
   resources->unload_all_resources();
}

int Kursk::main(int argc,char** argv)
{
	try
	{
		CL_Display::set_videomode(640,480,16,false);
		CL_Display::clear_display(0,0,0,1);

		CL_Surface* loading=CL_TargaProvider::create("Gfx/loading.tga",NULL);
		loading->put_screen(180,210);

		CL_Display::flip_display();
		CL_SetupCore::init_sound();
		//std::cout << "Initializing DDraw and setting the display for 640x480x16" << std::endl;

		try
		{
			resources = CL_ResourceManager::create("kursk.dat", true );
			//std::cout << "Opening the resource file from kursk.dat" << std::endl;
		}
		catch (CL_Error err)
		{
			resources = CL_ResourceManager::create("kursk.scr", false);
			//std::cout << "Using the source bitmaps without resource file" << std::endl;
		}

		init_resources();

		menu = new main_menu(this);
		menu->show_menu();
		
		deinit();
		delete resources;
		delete loading;
	}
	catch (CL_Error err)
	{
		//std::cout << "Error occured: " << err.message.c_str() << std::endl;
		return 1;
	}	
	return 0;
}	


